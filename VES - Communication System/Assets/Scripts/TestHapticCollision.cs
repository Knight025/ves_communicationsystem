﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestHapticCollision : MonoBehaviour
{
    public TestVirtualScenario test;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided");
        test.OnClickUpdateVibration();
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collided - exit");
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger - Enter");
        test.randomVibration.isOn = true;
        //test.OnClickUpdateVibration();
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Trigger - Exit");
        test.randomVibration.isOn = false;
    }
}
