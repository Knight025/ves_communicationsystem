﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct EnvelopingVASConfig
{
    public float detectionDistance; /*(Meters)*/
    public float period;   /*(Seconds)*/
}

public class EnvelopingVAS : MonoBehaviour {
    private EnvelopingVASConfig config;
    private bool testEnabled;
    VirtualScenarioManager.ChannelManager<HapticStimuli> channelManager;
    private HapticStimuli mainHapticStimuli;

    private GameObject RenderedFieldOfView;

    private bool firstCollision = true;

    // Use this for initialization
    public void Init(VirtualScenarioManager.ChannelManager<HapticStimuli> channelManager)
    {
        this.channelManager = channelManager;
        mainHapticStimuli = channelManager.AddHostScenarioElement((byte)HapticStimuli.SpawnableHapticStimuli.Default);
        //mainHapticStimuli.vibrationAmplitude = HapticStimuli.HapticEffect.drv2605L_effect_buzz5_20P;
        config.detectionDistance = 1.2f; //Default config
        config.period = 0.02f;

        this.gameObject.name = "ARGGHHHh";
        RenderedFieldOfView = GameObject.CreatePrimitive(PrimitiveType.Cube);
        RenderedFieldOfView.name = "Detection Area";
        RenderedFieldOfView.transform.localScale = new Vector3(0.02f, 0.02f, config.detectionDistance);
        RenderedFieldOfView.transform.localPosition = Vector3.forward* config.detectionDistance / 2;
        RenderedFieldOfView.transform.localRotation = Quaternion.identity;
        RenderedFieldOfView.transform.parent = transform;
        RenderedFieldOfView.GetComponent<MeshRenderer>().material = (Material)Resources.Load("Materials/TransparentRed"); // Assign a material for the renderer
        RenderedFieldOfView.SetActive(false);

        SetEnable(true);
    }

    //int count = 0;
    //public void Update()
    //{
    //    if (++count > 150)
    //    {
    //        mainHapticStimuli.effect++;
    //        count = 0;
    //        Debug.Log("Haptic Effect: " + mainHapticStimuli.effect.ToString());
    //    }
    //}

    private void EnvelopingVASTask()
    {
        int layerMask = LayerMask.GetMask("StimuliTrigger");
        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.forward*config.detectionDistance);
        if (Physics.Raycast(transform.position, transform.forward, out hit, config.detectionDistance, layerMask))
        {
            AudioMaterial audioMat;
            if ((audioMat = hit.transform.gameObject.GetComponent<AudioMaterial>()) == null)
            {
                Debug.Log("No audio material");
                return;
            }
            mainHapticStimuli.vibrationAmplitude = (byte) ((1 - hit.distance/config.detectionDistance)*128 + 128);
            if (firstCollision)
            {
                firstCollision = false;
                mainHapticStimuli.Impulse(200);
            }
            mainHapticStimuli.isOn = true;
        }
        else
        {
            firstCollision = true;
            //Debug.Log("Stopping vibration...");
            mainHapticStimuli.isOn = false;
        }
    }

    public VirtualScenarioManager.ChannelManager<HapticStimuli> GetChannelManager()
    {
        return channelManager;
    }

    public void DeInit()
    {
        channelManager.DestroyHostScenarioElement(mainHapticStimuli);
        Destroy(RenderedFieldOfView);
        Destroy(gameObject);
    }

    public void OnDestroy()
    {
        DeInit();
    }

    public void SetEnable(bool enable)
    {
        testEnabled = enable;
        if (enable)
        {
            Debug.Log("(EVAS) The test has just started");
            RenderedFieldOfView.SetActive(true);
            if (IsInvoking("EnvelopingVASTask"))
                CancelInvoke("EnvelopingVASTask");

            InvokeRepeating("EnvelopingVASTask", 0, config.period);
        }
        else
        {
            Debug.Log(" (PVAS) Stopping the test...");
            RenderedFieldOfView.SetActive(false);
            if (IsInvoking("EnvelopingVASTask"))
            {
                CancelInvoke("EnvelopingVASTask");
            }
        }
    }

    /* This function is called by the player through the GUI ('ProjectedVASGUI'), in order to make this aura "envelop and follow" him
     */
    public void AdoptMe(Transform parent)
    {
        transform.parent = parent.transform;
        transform.localEulerAngles = Vector3.zero;
        transform.localPosition = Vector3.zero;
    }
    
    public void SetConfig(EnvelopingVASConfig config)
    {
        this.config = config;
        RenderedFieldOfView.transform.localScale = new Vector3(0.02f, 0.02f, config.detectionDistance);
        RenderedFieldOfView.transform.localPosition = Vector3.forward * config.detectionDistance / 2;
    }

    public EnvelopingVASConfig GetConfig()
    {
        return config;
    }

    public bool IsTestEnabled()
    {
        return testEnabled;
    }
}
