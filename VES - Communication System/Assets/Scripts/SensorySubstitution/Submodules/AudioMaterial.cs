﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMaterial : MonoBehaviour {
    public VASAudioMaterial value;

    public void Start()
    {
        this.GetComponent<Renderer>().material = AudioMaterialMapping.GetPulseResponseData(value).material;
    }

    public void SetAudioMat(VASAudioMaterial value)
    {
        this.value = value;
    }

    public AudioMaterial(VASAudioMaterial value)
    {
        this.value = value;
    }
}