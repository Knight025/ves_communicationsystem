﻿using System.Collections.Generic;
using UnityEngine;
using System;


//NOTE: 'None' is used as 'Length' reference
public enum VASAudioMaterial
{
    Metal = 0,              /// <summary>
    Wood_1 = 1,             /// <summary>
    Wood_2 = 2,
    PaintedWall_1 = 3,
    PaintedWall_2 = 4,
    PaintedWall_3 = 5,
    Floor_1 = 6,
    Engine_1 = 7,
    Engine_2 = 8,
    None = 9
}


[Serializable]
public struct VASAudioSource
{
    public AudioClip audioClip;
    public float volume;
    public float pitch;
    public Material material;
    public VASAudioSource(AudioClip audioClip, float volume, float pitch, Material material)
    {
        this.audioClip = audioClip;
        this.volume = volume;
        this.pitch = pitch;
        this.material = material;
    }
}


public class AudioMaterialMapping {
    public static GameObject prefab = (GameObject)Resources.Load("Prefabs/AudioSource");                            // Audio source prefab

    private static Dictionary<VASAudioMaterial, VASAudioSource> audioClipsList = new Dictionary<VASAudioMaterial, VASAudioSource> {
        { VASAudioMaterial.Metal, new VASAudioSource((AudioClip)Resources.Load("Audio/05_MetalHit"), 1f, 1f, (Material) Resources.Load("Materials/Gray")) },
        { VASAudioMaterial.Wood_1, new VASAudioSource((AudioClip)Resources.Load("Audio/05_Wood_1Hit"), 1f, 1f, (Material) Resources.Load("Materials/Brown")) },
        { VASAudioMaterial.Wood_2, new VASAudioSource((AudioClip)Resources.Load("Audio/05_Wood_2Hit"), 1f, 1f, (Material) Resources.Load("Materials/Brown")) },
        { VASAudioMaterial.PaintedWall_1, new VASAudioSource((AudioClip)Resources.Load("Audio/02_WoodHit_1"), 0.4f, 1f, (Material) Resources.Load("Materials/Brown")) },
        { VASAudioMaterial.PaintedWall_2, new VASAudioSource((AudioClip)Resources.Load("Audio/02_WoodHit_2"), 0.4f, 1f, (Material) Resources.Load("Materials/Brown")) },
        { VASAudioMaterial.PaintedWall_3, new VASAudioSource((AudioClip)Resources.Load("Audio/02_WoodHit_3"), 0.4f, 1f, (Material) Resources.Load("Materials/Brown")) },
        { VASAudioMaterial.Floor_1, new VASAudioSource((AudioClip)Resources.Load("Audio/FastClick"), 0f, 1f, (Material) Resources.Load("Materials/Blue")) },
        { VASAudioMaterial.Engine_1, new VASAudioSource((AudioClip)Resources.Load("Audio/05_MetalHit"), 1f, 1f, (Material) Resources.Load("Materials/Black")) },
        { VASAudioMaterial.Engine_2, new VASAudioSource((AudioClip)Resources.Load("Audio/05_MetalHit"), 1f, 1f, (Material) Resources.Load("Materials/Black")) },
        //{ VASAudioMaterial.None, (AudioClip)Resources.Load("Audio/05_MetalHit") },
    };


    public static VASAudioSource GetPulseResponseData(VASAudioMaterial material)
    {
        if(!audioClipsList.TryGetValue(material, out VASAudioSource retval))
            throw new Exception("The material could not be found");
        return retval;
    }

    public static VASAudioSource GetCollisionVASAudio(VASAudioMaterial material)
    {
        if (!audioClipsList.TryGetValue(material, out VASAudioSource retval))
            throw new Exception("The material could not be found");
        return retval;
    }

    public static void SetPulseResponseData(VASAudioMaterial material, VASAudioSource data)
    {
        if (!audioClipsList.Remove(material))
            return;
        audioClipsList.Add(material, data);
    }

    public static GameObject GetAudioGO(VASAudioMaterial material, Vector3 position)
    {
        GameObject retval = GameObject.Instantiate(prefab, position, Quaternion.identity);
        AudioSource audioSource = retval.GetComponent<AudioSource>();

        VASAudioSource vasAudioSource;
        audioClipsList.TryGetValue(material, out vasAudioSource);
        audioSource.clip = vasAudioSource.audioClip;
        audioSource.pitch = vasAudioSource.pitch;
        audioSource.volume = vasAudioSource.volume;

        return retval;
    }
}