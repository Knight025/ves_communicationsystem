﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct ProjectedVASConfig
{
    public int M;                   //Stereopixels array - number of columns - use prime numbers
    public int N;                   //Stereopixels array - number of rows - use prime numbers
    public float fieldOfViewX;      //Horizontal field of view for acoustic image (stereopixels)
    public float fieldOfViewY;      //Vertical field of view which contains the stereopixels
    public float detectionDistance;
    public int randomizer;          //This integer serves for randomizing the sequence of stereopixels (nextIndex = randomizer%(M*N))
                                    //For this to work, SCM(randomizer, M, N) = 1 , where SCM is the smallest common multiple.
                                    //Therefore, prime numbers are preferred for M and N
    public float period;            //Stereopixels' spawning frequency in seconds. In the original VAS project, it was of 1 KHz aprox. (probably too fast for this design)
}

public class ProjectedVAS : MonoBehaviour {
    private AudioMaterial audioMat;
    private int counter;            //This 'counter' is used for randomizing the spawning order of the stereopixels
    private bool iTestEnabled;
    public bool testEnabled { set { EnableTest(value); } get { return iTestEnabled; } }
    //public GameObject prefab;
    private ProjectedVASConfig config;

    private GameObject RenderedFieldOfView;

    
    // Use this for initialization
    public void Init()
    {
        Debug.Log("Trying to start the projectedVAS!!!");
        //Default configuration
        config.M = 17;                              //VAS: 17
        config.N = 9;                               //VAS: 9
        config.fieldOfViewX = 50;                   //VAS: 80
        config.fieldOfViewY = 30;                   //VAS: 45
        config.detectionDistance = 20;              //VAS: infinite
        config.randomizer = 400;
        config.period = 0.02f;                      //VAS: tones of 1ms; 17x9 tones in 153 ms (1kHz)
        
        /*
         * The purpose of this code is to make visible the field of view of this test, but probably the easiest way to do that would require
         * including this script in the 'player' gameObject
         */
        RenderedFieldOfView = new GameObject("FieldOfView");
        RenderedFieldOfView.layer = 13;
        RenderedFieldOfView.transform.localPosition = Vector3.zero;
        RenderedFieldOfView.transform.localRotation = Quaternion.Euler(Vector3.zero);
        RenderedFieldOfView.transform.parent = this.transform;
        RenderedFieldOfView.AddComponent<MeshFilter>().sharedMesh = MeshGenerator.FieldOfViewMesh(config.detectionDistance, config.fieldOfViewX, config.fieldOfViewY);
        RenderedFieldOfView.AddComponent<MeshRenderer>().material = (Material)Resources.Load("Materials/Transparent"); // Assign a material for the renderer
        RenderedFieldOfView.SetActive(false);

        AdoptMe(GameObject.Find("Main Camera").transform);
        EnableTest(false);
    }

    public ProjectedVASConfig GetConfig()
    {
        return config;
    }

    public void SetConfig(ProjectedVASConfig config)
    {
        this.config = config;
    }

    public void EnableTest(bool enable)
    {
        iTestEnabled = enable;
        if (enable)
        {
            Debug.Log("(PVAS) The test has just started");
            if (IsInvoking("ProyectedVASTask"))
                CancelInvoke("ProyectedVASTask");

            counter = 0;
            RenderedFieldOfView.GetComponent<MeshFilter>().sharedMesh = MeshGenerator.FieldOfViewMesh(config.detectionDistance, config.fieldOfViewX, config.fieldOfViewY);
            RenderedFieldOfView.SetActive(true);
            InvokeRepeating("ProyectedVASTask", 0, config.period);
        }
        else
        {
            Debug.Log(" (PVAS) Stopping the test...");
            if (IsInvoking("ProyectedVASTask"))
            {
                RenderedFieldOfView.SetActive(false);
                CancelInvoke("ProyectedVASTask");
            }
        }
    }


    private Vector3 GetRay(float fieldOfViewX, float fieldOfViewY, int M, int N, int index)
    {
        return Quaternion.Euler((((float)(Mathf.Floor((float)index / M)) / (N - 1)) - 0.5f) * fieldOfViewY, (((float)(index % M) / (M - 1)) - 0.5f) * fieldOfViewX, 0) * Vector3.forward;
    }


    private void ProyectedVASTask()
    {
        int layerMask = LayerMask.GetMask("StimuliTrigger");
        RaycastHit hit;
        Vector3 direction = GetRay(config.fieldOfViewX, config.fieldOfViewY, config.M, config.N, (counter * config.randomizer) % (config.M * config.N));

        if ((++counter) == config.M * config.N)
            counter = 0;


        if (Physics.Raycast(this.transform.position, this.transform.TransformDirection(direction), out hit, config.detectionDistance, layerMask))
        {
            if ((audioMat = hit.transform.gameObject.GetComponent<AudioMaterial>()) == null)
            {
                Debug.Log("No audio material");
                return;
            }

            GameObject go = AudioMaterialMapping.GetAudioGO(audioMat.value, hit.point);
            go.GetComponent<ShortLivedAudioSource>().PlayAndDie(0.05f);
        }
    }

    /* This function is called by the player through the GUI ('ProjectedVASGUI'), in order to make this aura "envelop and follow" him
     */
    public void AdoptMe(Transform parent)
    {
        this.transform.parent = parent.transform;
        this.transform.localEulerAngles = Vector3.zero;
        this.transform.localPosition = Vector3.zero;
        RenderedFieldOfView.transform.localPosition = Vector3.zero;
        RenderedFieldOfView.transform.localEulerAngles = new Vector3(-90, 0, 0);
    }
    
    public void RenderFieldOfView(bool enable)
    {
        RenderedFieldOfView.SetActive(enable);
    }

    public bool IsTestEnabled()
    {
        return iTestEnabled;
    }
}
