﻿
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class TestVirtualScenario : MonoBehaviour
{
    NetworkManagerClient networkClient;
    NetworkManagerServer networkServer;
    VirtualScenarioManager virtualScenarioClient;
    VirtualScenarioManager virtualScenarioServer;
    VirtualScenarioManager.ChannelManager<PhysicalObject> physChannelManagerClient;
    VirtualScenarioManager.ChannelManager<PhysicalObject> physChannelManagerServer;
    VirtualScenarioManager.ChannelManager<AcousticStimuli> acChannelManagerClient;
    VirtualScenarioManager.ChannelManager<AcousticStimuli> acChannelManagerServer;
    VirtualScenarioManager.ChannelManager<HapticStimuli> hapChannelManagerClient;
    VirtualScenarioManager.ChannelManager<BodyPart> moCapChannel;
    MoCapManager moCapManager;

    public bool sendMsg2;
    public double lambda = 0.9;
    JitterModeling.PoissonDistribution jitterMod;
    JitterModeling.NoJitter jitterMod2;

    public GameObject target;
    public Text text;

    private PhysicalObject head;
    private AcousticStimuli randomSound;
    public HapticStimuli randomVibration;

    public static string stdOutput;

    // Start is called before the first frame update
    void Start()
    {
        sendMsg2 = false;
        networkServer = new NetworkManagerServer(11001, 11000, new NetworkManager.NetDeviceInfo(1, 0, true));
        networkClient = new NetworkManagerClient(11000, 11001);
        networkServer.Init();
        networkClient.Init();

        virtualScenarioClient = new VirtualScenarioManager(11002, 11003, networkClient.GetNetClock());
        virtualScenarioServer = new VirtualScenarioManager(11003, 11002, networkClient.GetNetClock());
        virtualScenarioServer.Init();
        virtualScenarioClient.Init();

        jitterMod = new JitterModeling.PoissonDistribution(lambda, 20);
        jitterMod2 = new JitterModeling.NoJitter();

        moCapManager = new MoCapManager(11004, 11005, virtualScenarioClient, 3, new JitterModeling.NoJitter());
        moCapManager.Init();

        virtualScenarioClient.AddChannel<PhysicalObject>(1, new JitterModeling.NoJitter(), out physChannelManagerClient);
        //physChannelManagerServer = virtualScenarioServer.AddChannel<PhysicalObject>(1, jitterMod);
        virtualScenarioServer.AddChannel<PhysicalObject>(1, new JitterModeling.NoJitter(), out physChannelManagerServer);
        virtualScenarioClient.AddChannel<AcousticStimuli>(2, new JitterModeling.NoJitter(), out acChannelManagerClient);
        virtualScenarioServer.AddChannel<AcousticStimuli>(2, new JitterModeling.NoJitter(), out acChannelManagerServer);
        virtualScenarioClient.AddChannel<HapticStimuli>(6, new JitterModeling.NoJitter(), out hapChannelManagerClient);
        moCapChannel = moCapManager.GetChannel();

        head = physChannelManagerServer.AddHostScenarioElement((int)PhysicalObject.SpawnableElement.Head);
        //head = physChannelManagerClient.AddHostScenarioElement((byte)PhysicalObject.SpawnableElement.Head);

        randomSound = acChannelManagerServer.AddHostScenarioElement((int)AcousticStimuli.SpawnableAudio.MetalHit);
        randomVibration = hapChannelManagerClient.AddHostScenarioElement((int)HapticStimuli.SpawnableHapticStimuli.Default);
        randomVibration.isOn = false;
        //randomSound.isPlaying = true;

        //moCapManager.AddBodyPart(BodyPart.SpawnableElement.Head, 1);
        //moCapManager.AddBodyPart(BodyPart.SpawnableElement.Arm, 1);

        //physChannelManagerClient.SubscribeNewDevice(IPAddress.Parse("192.168.1.46"));
        //physChannelManagerServer.SubscribeNewDevice(IPAddress.Parse("192.168.1.46"));
        
        
        
        //hapChannelManagerClient.SubscribeNewDevice(IPAddress.Parse("192.168.1.46"));
        hapChannelManagerClient.SubscribeNewDevice(IPAddress.Parse("192.168.1.126"));


        //jitterMod.StartRecording();
        Debug.Log("Path: " + Application.dataPath);
        //head.go.transform.parent = target.transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        jitterMod.lambda = lambda;

        virtualScenarioServer.UpdateHostScenario();
        virtualScenarioServer.UpdateRemoteScenario();
        virtualScenarioClient.UpdateHostScenario();
        virtualScenarioClient.UpdateRemoteScenario();

        moCapManager.UpdateBodyPose();

        head.pose = new Pose(target.transform.position,  target.transform.rotation);

        text.text = stdOutput;
    }

    public void OnDestroy()
    {
        networkClient.DeInit();
        networkServer.DeInit();
        virtualScenarioClient.DeInit();
        virtualScenarioServer.DeInit();
    }

    public void OnClickScanDevices()
    {
        networkClient.ScanNetDevices();
    }

    public void OnClickSubscribeToStream()
    {
        //List<IPAddress> list = networkClient.GetAvailableDevices();
        //Debug.Log("(Client) Subscribe button pulsed");
        //foreach (IPAddress devAddress in list)
        //{
        //    physChannelManagerClient.SubscribeTo(devAddress);
        //    acChannelManagerClient.SubscribeTo(devAddress);
        //    moCapChannel.SubscribeTo(devAddress);

        //    Debug.Log("(Client) Sent 'StartStreaming'");

        //    //Also subscribe these devices in this one
        //    physChannelManagerClient.SubscribeNewDevice(devAddress);
        //    acChannelManagerClient.SubscribeNewDevice(devAddress);
        //}
    }

    public void OnClickUnsubscribeFromStream()
    {
        //List<IPAddress> list = networkClient.GetAvailableDevices();

        //Debug.Log("Entered unsubscribe");
        //foreach (IPAddress devAddress in list)
        //{
        //    physChannelManagerClient.UnsubscribeFrom(devAddress);
        //    moCapChannel.UnsubscribeFrom(devAddress);
        //    Debug.Log("(Client) Sent 'StopStreaming'");
        //}
    }

    public void OnClickAddHead()
    {
        physChannelManagerServer.AddHostScenarioElement((int)PhysicalObject.SpawnableElement.Head);
    }

    public void OnClickRemoveRemote()
    {
        //List<IPAddress> list = networkClient.GetAvailableDevices();

        //foreach (IPAddress devAddress in list)
        //{
        //    physChannelManagerClient.DestroyRemoteScenarioElements(devAddress);
        //    Debug.Log("(Client) Data removed");
        //}
    }

    public void OnClickSyncClocks()
    {
        networkClient.SyncNetClocks();
    }

    public void OnClickCalibrateMoCap()
    {
        moCapManager.CalibrateBody(1);
    }

    public void OnClickUpdateVibration()
    {
        randomVibration.isOn = !randomVibration.isOn;
    }

    public void OnClickCalibrateIMUOffset()
    {
        //List<IPAddress> list = networkClient.GetAvailableDevices();
        //Debug.Log("(Client) IMU Offset calibration button pulsed");
        //foreach (IPAddress devAddress in list)
        //{
        //    moCapManager.CalibrateIMUOffset(devAddress);
        //}
        
    }

    public void OnDisable()
    {
        //jitterMod.StopRecordingAndSave(Application.dataPath);
    }
}
