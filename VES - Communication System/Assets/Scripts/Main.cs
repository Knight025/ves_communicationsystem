﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

[Serializable]
struct PowerConsumptionMeas
{
    public int timestamp;
    public uint powerConsumption;
    public PowerConsumptionMeas(int timestamp, uint powerConsumption)
    {
        this.timestamp = timestamp;
        this.powerConsumption = powerConsumption;
    }
}

public class Main : MonoBehaviour
{
    Session session;
    GameObject mainPanel;
    List<PowerConsumptionMeas> powerMeas;
    JsonManager<List<PowerConsumptionMeas>> powerConsumptionJM;

    // Start is called before the first frame update
    void Start()
    {
        Session.SessionInitData initData = new Session.SessionInitData()
        {
            netRemotePort   = 11000,
            netHostPort     = 11001,
            vsRemotePort    = 11002,
            vsHostPort      = 11003,
            moCapRemotePort = 11004,
            moCapHostPort   = 11005,
            mainMocapChannel = 3,
            pvasMocapChannel = 4,
            evasMocapChannel = 5,
            moCapBodyID     = 1,
        };

        session = new Session(initData);

        //jitterMod.StartRecording();
        Debug.Log("Path: " + Application.dataPath);

        /*This line limits the fps. Without it, the power consumption increases several fold*/
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
        
        mainPanel = GameObject.Find("MainPanel");
        mainPanel.GetComponent<MainGUI>().Init(session);

        //BodyPart head = session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.Head, 1);
        //head.go.transform.position = new Vector3(0, 1.6f, 0);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.Torso, 1);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.RightArm, 1);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.RightForearm, 1);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.LeftArm, 1);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.LeftForearm, 1);
        //session.moCapManager.AddBodyPart(BodyPart.SpawnableElement.Hip, 1).go.transform.position = new Vector3(0, 2, 0);

        powerMeas = new List<PowerConsumptionMeas>();
        powerConsumptionJM = new JsonManager<List<PowerConsumptionMeas>>("Power Consumption");

        InvokeRepeating("ScanNetwork", 1, 1);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        session.Update();
    }

    public void ScanNetwork()
    {
        session.ScanNetwork();
        //RecordPowerMeas();
    }

    void OnDestroy()
    {
        //SavePowerMeas();
        Destroy(mainPanel);
        session.DeInit();
    }

    public void RecordPowerMeas()
    {
        List<(IPAddress address, NetworkManager.NetDeviceInfo info)> connectedDevices = session.networkClient.GetAvailableDevices(Session.STAY_ALIVE_TIMEOUT);

        if (connectedDevices.Exists(p => p.info.deviceID == 4))
        {
            (IPAddress address, NetworkManager.NetDeviceInfo info) dev = connectedDevices.Find(p => p.info.deviceID == 4);
            PowerConsumptionMeas meas = new PowerConsumptionMeas(session.networkClient.GetNetClock().GetTimeStamp(), dev.info.batteryLevel);
            powerMeas.Add(meas);
        }
    }

    public void SavePowerMeas()
    {
        string filePath = Application.persistentDataPath + "/RecordingData";

        if (!Directory.Exists(filePath))
            Directory.CreateDirectory(filePath);
        powerConsumptionJM.SetData(powerMeas);
        powerConsumptionJM.SaveDataToFile("Power Consumption", filePath, false);
    }

    void OnDisable()
    {
        //jitterMod.StopRecordingAndSave(Application.dataPath);
    }

}
