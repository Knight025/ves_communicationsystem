﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

public class UdpManager : TransportLayer
{
    private Socket udpSocket_rx;
    private Socket udpSocket_tx;

    //RX thread constants
    private const int UDP_RX_BUF_LENGTH = 1024;

    public UdpManager(IPAddress ipAddress, int remotePort, int hostPort) : base(ipAddress, remotePort, hostPort)
    {

    }

    public UdpManager(IPAddress ipAddress, int remotePort, int hostPort, PkgReceivedCallback interruptCallback) : base(ipAddress, remotePort, hostPort, interruptCallback)
    {
        
    }

    public override void Init()
    {
        udpSocket_rx = new Socket(remoteEP.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
        udpSocket_tx = new Socket(remoteEP.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
        udpSocket_rx.EnableBroadcast = true;
        udpSocket_rx.MulticastLoopback = true;
        udpSocket_tx.EnableBroadcast = true;
        udpSocket_tx.MulticastLoopback = true;

        base.Init();
    }

    public bool SendPkgTo(IPEndPoint address, byte[] pkg)
    {
        txQueue.Add(new Tuple<byte[], IPEndPoint>(pkg, address));
        return true;
    }

    public bool SendPkgTo(IPAddress address, byte[] pkg)
    {
        txQueue.Add(new Tuple<byte[], IPEndPoint>(pkg, new IPEndPoint(address, remoteEP.Port)));
        return true;
    }

    public void SetRemoteEP(IPEndPoint ep)
    {
        remoteEP = ep;
    }

    public IPEndPoint GetRemoteEP()
    {
        return remoteEP;
    }

    public IPEndPoint GetHostEP()
    {
        return hostEP;
    }

    public override void ReceiverTask()
    {
        byte[] pkg;
        // Creates an IpEndPoint to capture the identity of the sending host.
        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint senderRemote = (EndPoint)sender;

        // Binding is required with ReceiveFrom calls.
        udpSocket_rx.Bind(hostEP);

        int length;
        byte[] rxBuf = new byte[UDP_RX_BUF_LENGTH];
        while (true)
        {
            // This call blocks. 
            length = udpSocket_rx.ReceiveFrom(rxBuf, SocketFlags.None, ref senderRemote);

            pkg = new byte[length];
            Array.Copy(rxBuf, pkg, length);
            if (interruptCallback != null)
            {
                interruptCallback(((IPEndPoint)senderRemote).Address, pkg);
            }
            else
            {
                rxQueue.Enqueue(pkg);
            }
        }
    }

    public override void SenderTask()
    {
        while (true)
        {
            Tuple<byte[], IPEndPoint> pkg = txQueue.Take();
            udpSocket_tx.SendTo(pkg.Item1, 0, pkg.Item1.Length, SocketFlags.None, pkg.Item2);
        }
    }

    public override void DeInit()
    {
        base.DeInit();
        udpSocket_rx.Close();
        udpSocket_tx.Close();
    }

}
