﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class TcpServerManager : TransportLayer
{
    Socket tcpListener;
    Socket tcpSocket;
    int maxConnections;

    //RX thread constants
    private const int TCP_RX_BUF_LENGTH = 256;

    public TcpServerManager(IPAddress ipAddress, int remotePort, int hostPort, int maxConnections) : base(ipAddress, remotePort, hostPort)
    {
        this.maxConnections = maxConnections;
    }

    public override void Init()
    {
        base.Init();
        tcpListener = new Socket(remoteEP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
    }

    public override void ReceiverTask()
    {
        byte[] pkg;
        int length;
        byte[] rxBuf = new byte[TCP_RX_BUF_LENGTH];

        tcpListener.Bind(hostEP);
        tcpListener.Listen(maxConnections);

        tcpSocket = tcpListener.Accept();

        while (true)
        {
            // This call blocks. 
            length = tcpSocket.Receive(rxBuf);
            pkg = new byte[length];
            Array.Copy(rxBuf, pkg, length);
            rxQueue.Enqueue(pkg);
        }

    }

    public override void SenderTask()
    {

    }
}
