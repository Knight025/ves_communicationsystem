﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Concurrent;
using System;

public abstract class TransportLayer
{
    public delegate void PkgReceivedCallback(IPAddress origin, byte[] data);

    public readonly IPAddress hostIP;

    protected IPEndPoint remoteEP;
    protected IPEndPoint hostEP;

    protected private Thread rxThread;
    protected Thread txThread;
    protected ConcurrentQueue<byte[]> rxQueue;
    protected BlockingCollection<Tuple<byte[], IPEndPoint>> txQueue;

    protected PkgReceivedCallback interruptCallback = null;

    public TransportLayer(IPAddress remoteIPAddress, int remotePort, int hostPort)
    {
        hostIP = GetHostLocalIP();
        if (hostIP == null)
            throw new Exception("Beware: could not get host IP");
        remoteEP = new IPEndPoint(remoteIPAddress, remotePort);
        hostEP = new IPEndPoint(hostIP, hostPort);
    }

    public TransportLayer(IPAddress remoteIPAddress, int remotePort, int hostPort, PkgReceivedCallback interruptCallback) : this(remoteIPAddress, remotePort, hostPort)
    {
        this.interruptCallback = interruptCallback;
    }

    public static IPAddress GetHostLocalIP()
    {
        IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
        
        foreach (IPAddress ad in ipEntry.AddressList)
        {
            if (ad.AddressFamily == AddressFamily.InterNetwork && (ad.GetAddressBytes()[0] == 192)) //BEWARE!!!!
                return ad;
        }
        return null;
    }

    public virtual void Init()
    {
        rxQueue = new ConcurrentQueue<byte[]>();
        txQueue = new BlockingCollection<Tuple<byte[], IPEndPoint>>();

        rxThread = new Thread(ReceiverTask);
        txThread = new Thread(SenderTask);
        rxThread.Priority = System.Threading.ThreadPriority.AboveNormal;
        txThread.Priority = System.Threading.ThreadPriority.AboveNormal;

        rxThread.Start();
        txThread.Start();
    }

    public virtual void DeInit()
    {
        rxThread.Abort();
        txThread.Abort();
    }

    public bool SendPkg(byte[] pkg)
    {
        txQueue.Add(new Tuple<byte[], IPEndPoint>(pkg, remoteEP));
        return true;
    }

    /*Only when no callback has been declared*/
    public bool ReceivePkg(ref byte[] pkg)
    {
        bool dataAvailable = (rxQueue.Count > 0);

        if (!dataAvailable)
            return false;

        if (!rxQueue.TryDequeue(out pkg))
            return false;

        return true;
    }

    public abstract void SenderTask();

    public abstract void ReceiverTask();

}