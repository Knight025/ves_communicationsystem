﻿using System;
using System.Net;
using UnityEngine;

public class NetworkManagerServer : NetworkManager
{
    private NetDeviceInfo devInfo;

    public NetworkManagerServer(int remoteNetComPort, int hostNetComPort, NetDeviceInfo devInfo) : base(remoteNetComPort, hostNetComPort)
    {
        this.devInfo = devInfo;
    }

    public override void ExecuteNetCommand(IPAddress origin, byte[] data)
    {
        Debug.Log("(Server) Command " + data[0] + " received from: " + origin.ToString());
        NetworkCommand command = (NetworkCommand)GetPkgCommand(data);
        switch (command)
        {
            case NetworkCommand.SyncClock:
                int timestamp = BitConverter.ToInt32(GetPkgData(data), 0);
                netClock.Restart(timestamp); //BEWARE with endian!!
                defaultIO.SendPkgTo(origin, BuildCommandPkg((byte)NetworkCommand.AqSyncClock));
                //Debug.Log("(Server) Sync Pkg Received at " + NetClock.GetTimeStamp());
                break;
            case NetworkCommand.GetNodeData:
                defaultIO.SendPkgTo(origin, BuildCommandPkg((byte)NetworkCommand.NewNodeData, devInfo.GetBytes()));
                //Debug.Log("(Server) Node data requested. Info: " + devInfo.ARCamPoseAvalailable + ", " + devInfo.SkeletonPoseAvailable);
                break;
        }
    }
}
