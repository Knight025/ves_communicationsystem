﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using UnityEngine;

public class MoCapManager : ApplicationLayer
{
    private VirtualScenarioManager vsManager;
    private VirtualScenarioManager.ChannelManager<BodyPart> mainChannelManager;
    private byte mainChannel;
    private List<Body> bodies;

    private struct CalibrationData
    {
        public Quaternion initialRotation;
        public Vector3 localRotationReference;
        public Vector3 globalRotationReference;
        public CalibrationData(Quaternion initialRotation, Vector3 localRotationReference, Vector3 globalRotationReference)
        {
            this.initialRotation = initialRotation;
            this.localRotationReference = localRotationReference;
            this.globalRotationReference = globalRotationReference;
        }

        public CalibrationData(AlignmentData data)
        {
            initialRotation = data.initialRotation;
            localRotationReference = data.localRotationReference;
            globalRotationReference = data.globalRotationReference;
        }

        public CalibrationData(byte[] data) : this()
        {
            int size = Marshal.SizeOf(this);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(data, 0, ptr, size);

            this = (CalibrationData)Marshal.PtrToStructure(ptr, GetType());
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] GetBytes()
        {
            int size = Marshal.SizeOf(this);
            byte[] retVal = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, retVal, 0, size);
            Marshal.FreeHGlobal(ptr);
            return retVal;
        }
    }

    private class Body
    {
        /*The first element is the skeleton's ID, while the second is the BodyPart*/
        public List<BodyPart> bodyParts;
        public byte bodyID;
        public byte channel;

        public Body(byte bodyID, byte channel)
        {
            bodyParts = new List<BodyPart>();
            this.bodyID = bodyID;
            this.channel = channel;
        }

        public bool AddBodyPart(BodyPart bodyPart)
        {
            /*Check if there is a bodyPart with the same prefabID*/
            if (bodyParts.Find(p => (p.prefabID == bodyPart.prefabID) && (p.channel == bodyPart.channel)) != null)
                return false;

            bodyParts.Add(bodyPart);

            foreach (BodyPart registeredBodyPart in bodyParts)
            {
                /*If one of the existing bodyParts is the 'pivot' of the new bodyPart, update it*/
                if (bodyPart.alignmentData != null && (registeredBodyPart.prefabID == (byte)bodyPart.alignmentData.pivotPrefab))
                {
                    bodyPart.pivot = registeredBodyPart.go.transform.Find(bodyPart.alignmentData.pivotName).gameObject;
                }

                /*If the new bodyPart is the 'pivot' of one of the existing remote elements, update it*/
                if (registeredBodyPart.alignmentData != null && ((byte)registeredBodyPart.alignmentData.pivotPrefab == bodyPart.prefabID))
                {
                    registeredBodyPart.pivot = bodyPart.go.transform.Find(registeredBodyPart.alignmentData.pivotName).gameObject;
                }
                
                    
            }

            return true;
        }

        public void UpdatePose()
        {
            foreach (BodyPart bodyPart in bodyParts)
            {
                /*If the body part has a pivot, update its position to match it*/
                if (!(bodyPart.pivot == null))
                    bodyPart.go.transform.position = bodyPart.pivot.transform.position;
            }
        }
    }

    public enum MoCapCommand
    {
        CalibrateBodyPart   = 0,
        CalibrateIMUOffset  = 1,
    }

    public MoCapManager(int remoteNetComPort, int hostNetComPort, VirtualScenarioManager vsManager, byte channel, JitterModeling jitterModelling) : base(remoteNetComPort, hostNetComPort)
    {
        this.vsManager = vsManager;
        this.mainChannel = channel;
        if (!vsManager.AddChannel<BodyPart>(channel, jitterModelling, InitRemoteBodyPart, out mainChannelManager))
            throw new Exception("The channel " + channel + " already exists");
        bodies = new List<Body>();
    }

    private void InitRemoteBodyPart(BodyPart bodyPart)
    {
        /*Search for a body which matches the bodyID. If it does not exist, create it*/
        Body body;
        if ((body = bodies.Find(p => p.bodyID == bodyPart.bodyID && p.channel == bodyPart.channel)) == null){
            body = new Body(bodyPart.bodyID, bodyPart.channel);
            bodies.Add(body);
        }
        
        if (body.AddBodyPart(bodyPart) == false)
            Debug.Log("The body part " + Enum.GetName(typeof(BodyPart.SpawnableElement), bodyPart.prefabID) + " was already instantiated");
    }

    public bool TryGetBodyParts(int bodyID, out List<BodyPart> bodyParts)
    {
        return TryGetBodyParts(bodyID, mainChannel, out bodyParts);
    }

    public bool TryGetBodyParts(int bodyID, byte channel, out List<BodyPart> bodyParts)
    {
        Body body;
        if ((body = bodies.Find(p => p.bodyID == bodyID && p.channel == channel)) == null)
        {
            bodyParts = null;
            return false;
        }
        bodyParts = body.bodyParts;
        return true;
    }

    public bool TryGetBodyParts(int bodyID, byte channel, out List<BodyPart> bodyParts, Predicate<BodyPart> predicate)
    {
        Body body;
        if ((body = bodies.Find(p => p.bodyID == bodyID && p.channel == channel)) == null)
        {
            bodyParts = null;
            return false;
        }
        bodyParts = body.bodyParts.FindAll(predicate);
        return true;
    }

    public BodyPart AddBodyPart(BodyPart.SpawnableElement prefabID, byte bodyID)
    {
        /*Search for a body which matches the bodyID. If it does not exist, create it*/
        Body body;
        if ((body = bodies.Find(p => p.bodyID == bodyID)) == null)
        {
            body = new Body(bodyID, mainChannel);
            bodies.Add(body);
        }

        BodyPart newBodyPart = mainChannelManager.AddHostScenarioElement((byte)prefabID);
        newBodyPart.bodyID = bodyID;
        if(!body.AddBodyPart(newBodyPart))
        {
            newBodyPart.DeInit();
            GC.SuppressFinalize(newBodyPart);    /*BEWARE!!!!!!!*/
            throw new Exception("The new bodyPart could not be loaded");
        }

        return newBodyPart;
    }

    public void UpdateBodyPose()
    {
        foreach(Body body in bodies)
        {
            body.UpdatePose();
        }
    }

    public VirtualScenarioManager.ChannelManager<BodyPart> GetChannel()
    {
        return mainChannelManager;
    }

    public VirtualScenarioManager.ChannelManager<BodyPart> AddMulticast(byte channel)
    {
        if (!vsManager.AddChannel<BodyPart>(channel, new JitterModeling.NoJitter(), InitRemoteBodyPart, out VirtualScenarioManager.ChannelManager<BodyPart> retval))
            throw new Exception("The channel " + channel + " already exists");
        mainChannelManager.SetMulticast(retval);
        return retval;
    }

    public void CalibrateBody(byte bodyID)
    {
        Body body;
        if((body = bodies.Find(p => p.bodyID == bodyID)) == null)
            throw new Exception("There is no body with bodyID " + bodyID);


        byte[] pkg;
        foreach (BodyPart bodyPart in body.bodyParts)
        {
            if (bodyPart.origin != defaultIO.hostIP && bodyPart.channel == mainChannel)
            {
                if(bodyPart.alignmentData != null)
                {
                    BodyPart head;
                    CalibrationData calibData = new CalibrationData(bodyPart.alignmentData);

                    /*If there is a 'head', use it as a calibration reference*/
                    if ((head = body.bodyParts.Find(p=>p.prefabID.Equals((int)BodyPart.SpawnableElement.Head))) != null)
                    {
                        Vector3 forward = new Vector3(head.go.transform.forward.x, 0, head.go.transform.forward.z);
                        calibData.globalRotationReference = Quaternion.FromToRotation(Vector3.forward, forward) * calibData.globalRotationReference;
                        calibData.initialRotation = Quaternion.FromToRotation(Vector3.forward, forward) * calibData.initialRotation;
                    }

                    /*Send "calibrate" commands to remote devices*/
                    pkg = BuildCommandPkg((byte)MoCapCommand.CalibrateBodyPart, calibData.GetBytes());
                    defaultIO.SendPkgTo(bodyPart.origin, pkg);
                }
            }
            else
            {
                /*THIS IS RESERVED FOR HOST ELEMENTS (for now it is not needed)*/
            }
        }
    }

    public void CalibrateIMUOffset(IPAddress address)
    {
        byte[] pkg = BuildCommandPkg((byte)MoCapCommand.CalibrateIMUOffset);
        defaultIO.SendPkgTo(address, pkg);
    }

    public override void ExecuteNetCommand(IPAddress origin, byte[] data)
    {
        /*THIS IS RESERVED FOR HOST ELEMENTS (for now it is not needed)*/

    }
}
