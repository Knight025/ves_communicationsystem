﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using UnityEngine;

public partial class VirtualScenarioManager
{
    public abstract class ChannelManager
    {
        protected private List<IPAddress> subscribedDevices;
        protected ConcurrentQueue<(byte[], IPAddress)> remoteScenarioUpdates;

        protected VirtualScenarioManager manager;
        public readonly byte channel;

        protected JitterManager jitterManager;
        protected RecordManager recordManager;

        protected List<ChannelManager> multicastChannels;

        public ChannelManager(VirtualScenarioManager manager, byte channel, JitterModeling jitterModeling)
        {
            remoteScenarioUpdates = new ConcurrentQueue<(byte[], IPAddress)>();
            subscribedDevices = new List<IPAddress>();
            this.manager = manager;
            this.channel = channel;
            recordManager = new RecordManager(channel);
            jitterManager = new JitterManager(recordManager, manager.netClock);
            multicastChannels = new List<ChannelManager>();
        }

        public ChannelManager(VirtualScenarioManager manager, byte channel) : this(manager, channel, new JitterModeling.NoJitter()) { }


        public void SubscribeNewDevice(IPAddress deviceAddress)
        {
            Monitor.Enter(subscribedDevices);
            if (!subscribedDevices.Any(p => p.Equals(deviceAddress)))
            {
                Debug.Log("Device added to channel " + channel + ": " + deviceAddress.ToString());
                subscribedDevices.Add(deviceAddress);
            }
            Monitor.Exit(subscribedDevices);
        }

        public void UnsubscribeDevice(IPAddress deviceAddress)
        {
            Monitor.Enter(subscribedDevices);
            if (subscribedDevices.Any(p => p.Equals(deviceAddress)))
            {
                subscribedDevices.Remove(deviceAddress);
            }
            Monitor.Exit(subscribedDevices);
        }

        public void UnsubscribeAllDevices()
        {
            Monitor.Enter(subscribedDevices);
            subscribedDevices.Clear();
            Monitor.Exit(subscribedDevices);
        }

        public void AddUpdate(byte[] data, IPAddress origin)
        {
            if (multicastChannels.Count > 0)
            {
                foreach (ChannelManager channelManager in multicastChannels) { channelManager.AddUpdate(data, origin); }
            }
            remoteScenarioUpdates.Enqueue((data, origin));
        }

        public void SubscribeTo(IPAddress deviceAddress)
        {
            manager.SendCommand(ChannelCommands.Subscribe, channel, deviceAddress);
        }

        public void UnsubscribeFrom(IPAddress deviceAddress)
        {
            manager.SendCommand(ChannelCommands.Unsubscribe, channel, deviceAddress);
        }

        public JitterManager GetJitterManager()
        {
            return jitterManager;
        }

        public RecordManager GetRecordManager()
        {
            return recordManager;
        }

        /* If this channelManager includes 'multicast' channelManagers, then all host elements are
         * also updated in the 'multicast' channelManagers*/
        public void SetMulticast(List<ChannelManager> channels)
        {
            multicastChannels.AddRange(channels);
        }

        public void SetMulticast(ChannelManager channel)
        {
            multicastChannels.Add(channel);
            subscribedDevices.Add(manager.defaultIO.hostIP);
        }

        public abstract void UpdateRemoteScenario();
        public abstract void UpdateHostScenario();
    }


    public class ChannelManager<T> : ChannelManager where T : ScenarioElement, new()
    {
        private List<T> remoteScenarioElements;
        private List<T> hostScenarioElements;
        public int HostElementsCount { get { return hostScenarioElements.Count; } }

        public delegate void newRemoteCallback(T obj);
        private newRemoteCallback remoteCallback = null;

        public ChannelManager(VirtualScenarioManager manager, byte channel, JitterModeling jitterModelling) : base(manager, channel, jitterModelling)
        {
            remoteScenarioElements = new List<T>();
            hostScenarioElements = new List<T>();
        }

        public ChannelManager(VirtualScenarioManager manager, byte channel, JitterModeling jitterModelling, newRemoteCallback remoteCallback) : this(manager, channel, jitterModelling)
        {
            this.remoteCallback = remoteCallback;
        }

        public T AddHostScenarioElement(byte prefabID)
        {
            byte counter = 1;

            /*Searches for a free ID. Specifically, it searches for previous host elements with the same prefabID and sets the 'number' variable accordingly.*/
            ulong ID = ScenarioElement.GenerateID(prefabID, counter, channel, manager.defaultIO.hostIP);
            Monitor.Enter(hostScenarioElements);
            while (hostScenarioElements.Find(p => p.elementID.Equals(ID)) != null)
            {
                ID = ScenarioElement.GenerateID(prefabID, ++counter, channel, manager.defaultIO.hostIP);
            }

            /*Creates the new element and adds it to the list*/
            T newElement = new T();
            newElement.InitHost(prefabID, counter, channel, manager.defaultIO.hostIP, manager.netClock, jitterManager.UpdateCriteria);
            hostScenarioElements.Add(newElement);
            //remoteCallback?.Invoke(newElement);
            Monitor.Exit(hostScenarioElements);

            return newElement;
        }

        public bool DestroyHostScenarioElement(T element)
        {
            element.DeInit();
            return hostScenarioElements.Remove(element);
        }

        public void DestroyRemoteScenarioElements(IPAddress address)
        {
            T currentElement;
            while ((currentElement = remoteScenarioElements.Find(p => p.origin.Equals(address))) != null)
            {
                remoteScenarioElements.Remove(currentElement);
                currentElement.DeInit();
                GC.SuppressFinalize(currentElement);    /*BEWARE!!!!!!!*/
            }
        }

        public override void UpdateRemoteScenario()
        {
            int availableData = remoteScenarioUpdates.Count;

            /*Dequeue all received data*/
            while (availableData-- > 0)
            {
                /*Dequeue the received data and the IP origin*/
                if (remoteScenarioUpdates.TryDequeue(out (byte[] data, IPAddress origin) update))
                {
                    /*Check if the corresponding ID already exists*/
                    ulong ID = new ScenarioElement.Header(update.data).elementID;
                    T element;
                    if ((element = remoteScenarioElements.Find(p => p.elementID.Equals(ID))) == null)
                    {
                        /*If it does not exist, create it*/
                        element = new T();
                        element.InitRemote(update.data, channel, update.origin, manager.netClock, jitterManager.UpdateCriteria);
                        remoteCallback?.Invoke(element);  /*It is used for further initialization of the new remote element outside ChannelManager*/
                        remoteScenarioElements.Add(element);
                    }
                    else
                    {
                        /*If it exists, add this data update to the corresponding buffer*/
                        element.AddUpdateToBuffer(update.data);
                    }
                }
            }

            /*Update ScenarioElement data*/
            foreach (T activeElement in remoteScenarioElements)
            {
                activeElement.Update();
            }
        }

        public override void UpdateHostScenario()
        {
            if (subscribedDevices.Count == 0)
                return;

            Monitor.Enter(hostScenarioElements);
            foreach (T activeElement in hostScenarioElements)
            {
                if(activeElement.UpdateAvailable(out byte[] data))
                {
                    Monitor.Enter(subscribedDevices);
                    foreach (IPAddress address in subscribedDevices)
                    {
                        //Debug.Log("Updating subscriptor " + address.ToString() + " data.");
                        manager.SendCommand(ChannelCommands.ElementUpdate, channel, address, data);
                        recordManager.RecordHostUpdate(activeElement.elementID, data, manager.netClock.GetTimeStamp());
                    }
                    Monitor.Exit(subscribedDevices);

                    if (multicastChannels.Count > 0)
                    {
                        foreach (ChannelManager channelManager in multicastChannels) {
                            channelManager.AddUpdate(data, manager.defaultIO.hostIP);
                            recordManager.RecordHostUpdate(activeElement.elementID, data, manager.netClock.GetTimeStamp());
                        }
                    }
                }
            }
            Monitor.Exit(hostScenarioElements);
        }
    }
}

