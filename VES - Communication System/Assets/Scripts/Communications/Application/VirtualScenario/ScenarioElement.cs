﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using UnityEngine;


public abstract class ScenarioElement
{
    public GameObject go = null;
    private SortedDictionary<int, byte[]> updateBuffer;
    private NetClock netClock;

    //General data
    public ulong elementID;
    public IPAddress origin;
    public byte prefabID;
    public byte instanceCounter;
    public byte channel;

    public delegate void UpdateFunc(byte[] data);
    public delegate void UpdateCriteriaFunc(ulong elementID, SortedDictionary<int, byte[]> updateBuffer, UpdateFunc config);

    protected UpdateCriteriaFunc updateCriteria;

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Header
    {
        public ulong elementID;
        public byte prefabID;
        public byte instanceCounter;
        public int timeStamp;
        
        public Header(byte prefabID, byte instanceCounter, int timeStamp, ulong elementID)
        {
            this.prefabID = prefabID;
            this.instanceCounter = instanceCounter;
            this.timeStamp = timeStamp;
            this.elementID = elementID;
        }

        public Header(byte[] arr) : this()
        {
            int size = Marshal.SizeOf(this);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            this = (Header)Marshal.PtrToStructure(ptr, GetType());
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] GetBytes()
        {
            int size = Marshal.SizeOf(this);
            byte[] retVal = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, retVal, 0, size);
            Marshal.FreeHGlobal(ptr);
            return retVal;
        }
    }

    public static byte[] GetUpdateData(byte[] pkg)
    {
        int headerSize = Marshal.SizeOf(typeof(Header));
        if (pkg.Length <= headerSize)
            return null;
        byte[] retVal = new byte[pkg.Length - headerSize];
        Array.Copy(pkg, headerSize, retVal, 0, retVal.Length);
        return retVal;
    }


    public ScenarioElement()
    {
        updateBuffer = new SortedDictionary<int, byte[]>();
    }

    private void Init(byte prefabID, byte instanceCounter, byte channel, IPAddress origin, ulong elementID, NetClock netClock, UpdateCriteriaFunc updateCriteria)
    {
        this.origin = origin;
        this.prefabID = prefabID;
        this.instanceCounter = instanceCounter;
        this.updateCriteria = updateCriteria;
        this.channel = channel;
        this.elementID = elementID;
        this.netClock = netClock;
        //Debug.Log("PrefabID: " + prefabID + ", instanceCounter: " + instanceCounter + ", channel: " + channel + ", origin: " + origin.ToString());

        LoadPrefab(prefabID);
    }

    /*This function is meant to initialize host elements*/
    public virtual void InitHost(byte prefabID, byte instanceCounter, byte channel, IPAddress origin, NetClock netClock, UpdateCriteriaFunc updateCriteria)
    {
        Init(prefabID, instanceCounter, channel, origin, GenerateID(prefabID, instanceCounter, channel, origin), netClock, updateCriteria);
    }

    /*This function is meant to initialize remote elements*/
    public void InitRemote(byte[] data, byte channel, IPAddress origin, NetClock netClock, UpdateCriteriaFunc updateCriteria)
    {
        Header header = new Header(data);
        Init(header.prefabID, header.instanceCounter, channel, origin, header.elementID, netClock, updateCriteria);

        Config(GetUpdateData(data));
    }

    public void DeInit()
    {
        if (go != null)
            UnityEngine.Object.Destroy(go);
    }

    public void Update()
    {
        updateCriteria(elementID, updateBuffer, Config);
    }

    public void AddUpdateToBuffer(byte[] data)
    {
        Header aux = new Header(data);
        if (updateBuffer.ContainsKey(aux.timeStamp))
        {
            UnityEngine.Debug.Log("Same timestamp lol. This should not happen, as there cannot be two data updates at the same time");
            UnityEngine.Debug.Log("Channel: " + channel);
            return;
        }
        updateBuffer.Add(aux.timeStamp, GetUpdateData(data));
    }

    private Header BuildHeader()
    {
        return new Header(prefabID, instanceCounter, netClock.GetTimeStamp(), elementID);
    }

    protected byte[] BuildPkg(byte[] data)
    {
        byte[] header = BuildHeader().GetBytes();
        byte[] pkg = new byte[header.Length + data.Length];

        System.Buffer.BlockCopy(header, 0, pkg, 0, header.Length);
        System.Buffer.BlockCopy(data, 0, pkg, header.Length, data.Length);

        return pkg;
    }

    public static ulong GenerateID(byte prefabID, byte number, byte channel, IPAddress origin)
    {
        //long aux = origin.GetAddressBytes()[3] + origin.GetAddressBytes()[2]*8 + origin.GetAddressBytes()[1]*64 + origin.GetAddressBytes()[0]*512 + prefabID*4096 + number*32768;
        //Debug.Log("ID: " + aux + ", prefabID: " + prefabID + ", instanceCounter: " + number + ", channel: " + channel + ", origin: " + origin.ToString());
        return (ulong)(origin.GetAddressBytes()[3] + origin.GetAddressBytes()[2] * 8 + origin.GetAddressBytes()[1] * 64 + origin.GetAddressBytes()[0] * 512 + prefabID * 4096 + number * 32768);
    }

    
    protected abstract void LoadPrefab(byte prefabID);

    public abstract bool UpdateAvailable(out byte[] data);

    protected abstract void Config(byte[] data);
}

