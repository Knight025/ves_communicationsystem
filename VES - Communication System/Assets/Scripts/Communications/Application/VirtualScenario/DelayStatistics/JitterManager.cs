﻿using System.Collections.Generic;
using UnityEngine;

public class JitterManager
{
    private JitterModeling jitterModelling;
    private RecordManager recordManager;
    private NetClock netClock;

    public JitterManager(RecordManager recordManager, NetClock netClock, JitterModeling jitterModelling)
    {
        this.recordManager = recordManager;
        this.netClock = netClock;
        SetJitterModelling(jitterModelling);
    }

    public JitterManager(RecordManager recordManager, NetClock netClock) : this(recordManager, netClock, new JitterModeling.NoJitter()) { }

    public void SetJitterModelling(JitterModeling jitterModelling)
    {
        jitterModelling.Init(netClock, recordManager);
        this.jitterModelling = jitterModelling;
    }

    public JitterModeling GetJitterModeling()
    {
        return jitterModelling;
    }

    public void UpdateCriteria(ulong elementID, SortedDictionary<int, byte[]> updateBuffer, ScenarioElement.UpdateFunc update)
    {
        jitterModelling.UpdateCriteria(elementID, updateBuffer, update);
    }
}
