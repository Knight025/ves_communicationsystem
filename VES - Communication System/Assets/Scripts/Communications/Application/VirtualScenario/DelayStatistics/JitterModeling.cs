﻿using System;
using System.Collections.Generic;
using System.Linq;

public abstract class JitterModeling
{
    private RecordManager recordManager;
    private NetClock netClock;

    public JitterModeling() { }

    public void Init(NetClock netClock, RecordManager recordManager)
    {
        this.netClock = netClock;
        this.recordManager = recordManager;
    }

    public abstract void UpdateCriteria(ulong elementID, SortedDictionary<int, byte[]> updateBuffer, ScenarioElement.UpdateFunc update);

    public class NoJitter : JitterModeling
    {
        public NoJitter() : base() { }

        public override void UpdateCriteria(ulong elementID, SortedDictionary<int, byte[]> updateBuffer, ScenarioElement.UpdateFunc update)
        {
            int currentTime = netClock.GetTimeStamp();
            while (updateBuffer.Count > 0)
            {
                var firstPkg = updateBuffer.First();
                if (currentTime < firstPkg.Key)
                    return;
                update(firstPkg.Value);
                recordManager.RecordRemoteUpdate(elementID, (byte[])firstPkg.Value.Clone(), firstPkg.Key, currentTime);
                updateBuffer.Remove(firstPkg.Key);
            }
        }
    }

    public class PoissonDistribution : JitterModeling
    {
        private System.Random random;
        private double iLambda;  /*Internal value of 'lambda'*/
        private int bufferSize;

        private double[] distributionFunc;

        public double lambda
        {
            set { if (value < 0) throw new ArgumentOutOfRangeException("Landa out of bounds"); iLambda = value; LoadDistributionFunc(); }
            get { return iLambda; }
        }

        public PoissonDistribution(double lambda, int bufferSize) : base()
        {
            if(lambda < 0)
                throw new ArgumentOutOfRangeException("Landa out of bounds");
            iLambda = lambda;
            this.bufferSize = bufferSize;
            random = new System.Random();

            LoadDistributionFunc();
        }

        private void LoadDistributionFunc()
        {
            distributionFunc = new double[bufferSize];

            distributionFunc[0] = ProbDensityFunc(0);
            for (int i = 1; i < bufferSize-1; i++)
            {
                distributionFunc[i] = distributionFunc[i - 1] + ProbDensityFunc(i);
            }
            distributionFunc[bufferSize - 1] = 1;
        }

        private double ProbDensityFunc(int k)
        {
            /*Calculate factorial*/
            double factorial = k == 0? 1 : k;
            int aux = k;
            while (aux > 2)
                factorial *= (--aux);
            /*Return fdp*/
            return Math.Exp(-lambda) * Math.Pow(lambda, k) / factorial;
        }

        private int Poisson()
        {
            double aux = random.NextDouble();
            return Array.FindIndex(distributionFunc, val => val >= aux); /*Returns the first match*/
        }

        public override void UpdateCriteria(ulong elementID, SortedDictionary<int, byte[]> updateBuffer, ScenarioElement.UpdateFunc update)
        {
            if (updateBuffer.Count > 0)
            {
                int pkgsReady;
                int currentTime = netClock.GetTimeStamp();
                if ((pkgsReady = updateBuffer.Keys.Count(timestamp => timestamp <= currentTime)) == 0)
                    return;

                /*Get the number of packages to be removed from the queue. It is the minimum of Poisson() and the available packages*/
                int pkgsToUpdate = Math.Min(Poisson(), pkgsReady);

                KeyValuePair<int, byte[]> firstEntry;
                while (pkgsToUpdate-- > 0)
                {
                    firstEntry = updateBuffer.First();
                    //RecordUpdate(new RegisterEntry(ID, firstEntry.Key, NetClock.GetTimeStamp()));
                    if (recordManager != null)
                        recordManager.RecordRemoteUpdate(elementID, (byte[])firstEntry.Value.Clone(), firstEntry.Key, netClock.GetTimeStamp());
                    update(firstEntry.Value);
                    updateBuffer.Remove(firstEntry.Key);
                }
            }
        }
    }
}
