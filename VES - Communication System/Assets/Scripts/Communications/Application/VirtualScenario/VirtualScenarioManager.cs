﻿using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public partial class VirtualScenarioManager : ApplicationLayer
{
    private Dictionary<byte, ChannelManager> activeChannels;
    private NetClock netClock;

    public enum ChannelCommands
    {
        Subscribe       = 0,
        Unsubscribe     = 1,
        ElementUpdate   = 2,
    }

    public VirtualScenarioManager(int remoteNetComPort, int hostNetComPort, NetClock netClock) : base(remoteNetComPort, hostNetComPort)
    {
        activeChannels = new Dictionary<byte, ChannelManager>();
        this.netClock = netClock;
    }

    public bool AddChannel<T>(byte channel, JitterModeling jitterModelling, ChannelManager<T>.newRemoteCallback callback, out ChannelManager<T> channelManager) where T : ScenarioElement, new()
    {
        if (activeChannels.ContainsKey(channel))
        {
            channelManager = null;
            return false;
        }
        channelManager = new ChannelManager<T>(this, channel, jitterModelling, callback);
        activeChannels.Add(channel, channelManager);
        return true;
    }

    public bool AddChannel<T>(byte channel, JitterModeling jitterModelling, out ChannelManager<T> channelManager) where T : ScenarioElement, new()
    {
        return AddChannel<T>(channel, jitterModelling, null, out channelManager);
    }

    public bool AddChannel<T>(byte channel, out ChannelManager<T> channelManager) where T : ScenarioElement, new()
    {
        return AddChannel<T>(channel, new JitterModeling.NoJitter(), null, out channelManager);
    }

    public ChannelManager GetChannelManager(byte channel)
    {
        return activeChannels[channel];
    }

    public void RemoveChannel(byte channel)
    {
        activeChannels.Remove(channel);
    }

    public void SendCommand(ChannelCommands command, byte channel, IPAddress address)
    {
        byte[] data = new byte[] { channel };
        defaultIO.SendPkgTo(address, BuildCommandPkg((byte)command, data));
    }

    public void SendCommand(ChannelCommands command, byte channel, IPAddress address, byte[] data)
    {
        byte[] txData = new byte[1 + data.Length];

        txData[0] = (byte)channel;
        System.Buffer.BlockCopy(data, 0, txData, 1, data.Length);
        defaultIO.SendPkgTo(address, BuildCommandPkg((byte)command, txData));
    }


    public void UpdateRemoteScenario()
    {
        foreach (ChannelManager channelManager in activeChannels.Values)
        {
            channelManager.UpdateRemoteScenario();
        }
    }

    public void UpdateHostScenario()
    {
        foreach (ChannelManager channelManager in activeChannels.Values)
        {
            channelManager.UpdateHostScenario();
        }
    }

    //public void StartRecording()
    //{
    //    foreach(KeyValuePair<byte, ChannelManager> entry in activeChannels)
    //    {
    //        entry.Value.GetRecordManager().StartRecording();
    //    }
    //}

    private static byte GetPkgChannel(byte[] data)
    {
        return data[0];
    }

    private static byte[] GetChannelData(byte[] data)
    {
        if (data.Length <= 1)
            return null;
        byte[] retVal = new byte[data.Length - 1];
        Array.Copy(data, 1, retVal, 0, retVal.Length);
        return retVal;
    }

    public override void ExecuteNetCommand(IPAddress origin, byte[] data)
    {
        ChannelCommands command = (ChannelCommands)GetPkgCommand(data);
        byte[] channelPkg = GetPkgData(data);
        byte channel = GetPkgChannel(channelPkg);
        //Debug.Log("(Virtual Scenario Manager) " + this.defaultIO.GetHostEP().Port + " Command " + data[0] + ". Channel: " + channel + " received from: " + origin.ToString());
        if (activeChannels.TryGetValue(channel, out ChannelManager channelManager))
        {
            switch (command)
            {
                case ChannelCommands.ElementUpdate:
                    channelManager.AddUpdate(GetChannelData(channelPkg), origin);
                    break;
                case ChannelCommands.Subscribe:
                    channelManager.SubscribeNewDevice(origin);
                    break;
                case ChannelCommands.Unsubscribe:
                    channelManager.UnsubscribeDevice(origin);
                    break;
            }
        }
    }
}
