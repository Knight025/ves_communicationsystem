﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class BodyPart : ScenarioElement
{
    public byte bodyID;
    public GameObject pivot = null;
    public AlignmentData alignmentData = null;
    public UpdateType updateType = UpdateType.PositionAndOrientation;

    public Pose pose
    {
        set { go.transform.position = value.position; go.transform.rotation = value.rotation; }
        get { return new Pose(go.transform.position, go.transform.rotation); }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct PkgData
    {
        public byte updateType;
        public byte bodyID;
        public Pose pose;

        public PkgData(UpdateType updateType, byte bodyID, Pose pose)
        {
            this.updateType = (byte)updateType;
            this.bodyID = bodyID;
            this.pose = pose;
        }

        public PkgData(byte[] data) : this()
        {
            int size = Marshal.SizeOf(this);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(data, 0, ptr, size);

            this = (PkgData)Marshal.PtrToStructure(ptr, GetType());
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] GetBytes()
        {
            int size = Marshal.SizeOf(this);
            byte[] retVal = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, retVal, 0, size);
            Marshal.FreeHGlobal(ptr);
            return retVal;
        }
    }

    public enum UpdateType
    {
        PositionAndOrientation = 0,
        Orientation = 1,
    }


    public enum SpawnableElement
    {
        Head = 0,
        Torso = 1,
        RightArm = 2,
        LeftArm = 3,
        RightForearm = 4,
        LeftForearm = 5,
        RightThigh = 6,
        LeftThigh = 7,
        RightCalf = 8,
        LeftCalf = 9,
        Hip = 10
    }

    //public enum SpawnableElement
    //{
    //    Head = 10,
    //    Torso = 1,
    //    RightArm = 6,
    //    LeftArm = 7,
    //    RightForearm = 8,
    //    LeftForearm = 9,
    //    RightThigh = 2,
    //    LeftThigh = 3,
    //    RightCalf = 4,
    //    LeftCalf = 5,
    //    Hip = 0
    //}

    private static readonly Dictionary<SpawnableElement, GameObject> spawnableElements = new Dictionary<SpawnableElement, GameObject> {
        { SpawnableElement.Head, (GameObject)Resources.Load("Prefabs/Skeleton/Head") },
        { SpawnableElement.Torso, (GameObject)Resources.Load("Prefabs/Skeleton/Torso") },
        { SpawnableElement.RightArm, (GameObject)Resources.Load("Prefabs/Skeleton/RightArm") },
        { SpawnableElement.LeftArm, (GameObject)Resources.Load("Prefabs/Skeleton/LeftArm") },
        { SpawnableElement.RightForearm, (GameObject)Resources.Load("Prefabs/Skeleton/RightForearm") },
        { SpawnableElement.LeftForearm, (GameObject)Resources.Load("Prefabs/Skeleton/LeftForearm") },
        { SpawnableElement.RightThigh, (GameObject)Resources.Load("Prefabs/Skeleton/RightThigh") },
        { SpawnableElement.LeftThigh, (GameObject)Resources.Load("Prefabs/Skeleton/LeftThigh") },
        { SpawnableElement.RightCalf, (GameObject)Resources.Load("Prefabs/Skeleton/RightCalf") },
        { SpawnableElement.LeftCalf, (GameObject)Resources.Load("Prefabs/Skeleton/LeftCalf") },
        { SpawnableElement.Hip, (GameObject)Resources.Load("Prefabs/Skeleton/Hip") },
    };

    public BodyPart() : base() {
        
    }

    protected override void LoadPrefab(byte prefabID)
    {
        if (spawnableElements.TryGetValue((SpawnableElement)prefabID, out GameObject prefab))
        {
            go = MonoBehaviour.Instantiate(prefab);
            //go.name = Enum.GetName(typeof(SpawnableElement), prefabID) + "_" + elementID.ToString();
            go.name = Enum.GetName(typeof(SpawnableElement), prefabID) + "_" + bodyID + "_" + channel;
            alignmentData = go.GetComponent<AlignmentData>(); /*NOTE: not all spawnableElements have alignmentData*/
            //Debug.Log("NEW BODYPART CREATED!!!!");
        }
        else { throw new Exception("The item with prefabID " + prefabID + " could not be loaded"); }
    }

    protected override void Config(byte[] updateData)
    {
        PkgData data = new PkgData(updateData);
        if (data.updateType == (byte)UpdateType.PositionAndOrientation)
            go.transform.position = data.pose.position;
        go.transform.rotation = data.pose.rotation;
        if(bodyID != data.bodyID)
        {
            bodyID = data.bodyID;
            go.name = Enum.GetName(typeof(SpawnableElement), prefabID) + "_" + bodyID + "_" + channel;
        }
    }

    public override bool UpdateAvailable(out byte[] data)
    {
        data = BuildPkg(new PkgData(updateType, bodyID,  new Pose(go.transform.position, go.transform.rotation)).GetBytes());
        return true;
    }
}
