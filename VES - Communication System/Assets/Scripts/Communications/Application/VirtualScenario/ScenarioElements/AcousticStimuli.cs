﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using UnityEngine;


public class AcousticStimuli : ScenarioElement
{
    private bool propertiesModified;
    private AudioSource audioSource;

    public Pose pose
    {
        set { go.transform.position = value.position; go.transform.rotation = value.rotation; propertiesModified = true; }
        get { return new Pose(go.transform.position, go.transform.rotation); }
    }

    public bool isPlaying
    {
        set { if (value && !audioSource.isPlaying) { audioSource.Play(); } if (!value && audioSource.isPlaying) { audioSource.Stop(); } propertiesModified = true; }
        get { return audioSource.isPlaying; }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct PkgData
    {
        public Pose pose;
        public bool isPlaying;
        public PkgData(Pose pose, bool isPlaying)
        {
            this.pose = pose;
            this.isPlaying = isPlaying;
        }

        public PkgData(byte[] data) : this()
        {
            int size = Marshal.SizeOf(this);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(data, 0, ptr, size);

            this = (PkgData)Marshal.PtrToStructure(ptr, GetType());
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] GetBytes()
        {
            int size = Marshal.SizeOf(this);
            byte[] retVal = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, retVal, 0, size);
            Marshal.FreeHGlobal(ptr);
            return retVal;
        }
    }

    public enum SpawnableAudio
    {
        MetalHit = 1,
    }

    public AcousticStimuli() : base() { }


    private static readonly Dictionary<int, AudioClip> spawnableAudio = new Dictionary<int, AudioClip> {
        { (int)SpawnableAudio.MetalHit, (AudioClip)Resources.Load("Audio/05_MetalHit") },
    };

    //public AcousticStimuli(byte prefabID, byte number, byte channel, IPAddress origin, UpdateCriteriaFunc updateCriteria) : base()
    //{
    //    Init(prefabID, number, channel, origin, updateCriteria);
    //}

    protected override void LoadPrefab(byte prefabID)
    {
        if(spawnableAudio.TryGetValue(prefabID, out AudioClip clip))
        {
            go = new GameObject(elementID.ToString());
            audioSource = go.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.loop = true;
        }
    }

    protected override void Config(byte[] updateData)
    {
        //throw new NotImplementedException();
        PkgData remote = new PkgData(updateData);
        go.transform.position = remote.pose.position;
        go.transform.rotation = remote.pose.rotation;
        if (remote.isPlaying && !audioSource.isPlaying)
        {
            audioSource.Play();
        }
        if(!remote.isPlaying && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    public override bool UpdateAvailable(out byte[] data)
    {
        if (propertiesModified)
        {
            propertiesModified = false;
            data = BuildPkg(new PkgData(new Pose(go.transform.position, go.transform.rotation), isPlaying).GetBytes());
            return true;
        }
        data = null;
        return false;
    }
}

