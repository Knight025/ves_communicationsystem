﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public struct Pose
{
    public Vector3 position;
    public Quaternion rotation;
    
    public Pose(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }

    public Pose(byte[] data) : this()
    {
        int size = Marshal.SizeOf(this);
        IntPtr ptr = Marshal.AllocHGlobal(size);

        Marshal.Copy(data, 0, ptr, size);

        this = (Pose)Marshal.PtrToStructure(ptr, GetType());
        Marshal.FreeHGlobal(ptr);
    }

    public byte[] GetBytes()
    {
        int size = Marshal.SizeOf(this);
        byte[] retVal = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(this, ptr, true);
        Marshal.Copy(ptr, retVal, 0, size);
        Marshal.FreeHGlobal(ptr);
        return retVal;
    }
};

public struct SkeletonPose
{
    public Quaternion torsoRotation;
    public Quaternion armRotation;
    public Quaternion forearmRotation;
    public SkeletonPose(Quaternion torsoRotation, Quaternion armRotation, Quaternion forearmRotation)
    {
        this.torsoRotation = torsoRotation;
        this.armRotation = armRotation;
        this.forearmRotation = forearmRotation;
    }
};

public class Serializer
{
    public static byte[] GetBytes(object str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }

    public static T GetFromBytes<T>(byte[] arr)
    {
        // Pin the managed memory while, copy it out the data, then unpin it
        GCHandle handle = GCHandle.Alloc(arr, GCHandleType.Pinned);
        T theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
        handle.Free();

        return theStructure;
    }



    public static Pose GetPose(byte[] arr)
    {
        Pose str = new Pose();

        int size = Marshal.SizeOf(str);
        IntPtr ptr = Marshal.AllocHGlobal(size);

        Marshal.Copy(arr, 0, ptr, size);

        str = (Pose)Marshal.PtrToStructure(ptr, str.GetType());
        Marshal.FreeHGlobal(ptr);
        return str;
    }


    public static SkeletonPose GetSkeletonPose(byte[] arr)
    {
        SkeletonPose str = new SkeletonPose();

        int size = Marshal.SizeOf(str);
        IntPtr ptr = Marshal.AllocHGlobal(size);

        Marshal.Copy(arr, 0, ptr, size);

        str = (SkeletonPose)Marshal.PtrToStructure(ptr, str.GetType());
        Marshal.FreeHGlobal(ptr);

        return str;
    }

    public static byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);

        return ms.ToArray();
    }

    // Convert a byte array to an Object
    public static object ByteArrayToObject(byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        object obj = (object)binForm.Deserialize(memStream);

        return obj;
    }
}
