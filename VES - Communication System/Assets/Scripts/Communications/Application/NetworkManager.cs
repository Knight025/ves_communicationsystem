﻿using System;
using System.Net;
using System.Runtime.InteropServices;

public abstract class NetworkManager : ApplicationLayer
{
    //protected static readonly IPAddress DEFAULT_NET_COM_ADDRESS = IPAddress.Parse("192.168.1.0");
    //protected static readonly IPAddress DEFAULT_NET_MASK = IPAddress.Parse("255.255.255.0");
    protected NetClock netClock;

    protected enum NetworkCommand
    {
        GetNodeData = 0,    //Upstream
        SyncClock = 1,      //Upstream
        NewNodeData = 2,    //Downstream
        AqSyncClock = 3,    //Downstream
    }

    [Serializable]
    protected class NetDevice
    {
        public readonly IPAddress address;
        public NetDeviceInfo info;
        public int timestamp_dataReceived;

        public static readonly int MIN_CLOCK_ACCURACY = 12; //2-5
        public int timeStamp_SyncSent;  //BEWARE!! This code is temporary, only for test purposes.
        public int timeStamp_SyncReceived;

        //public bool isSynchronized { get { throw new NotImplementedException("I cannot detect this yet");  return GetOffsetAccuracy() < MIN_CLOCK_ACCURACY; } }

        public NetDevice(IPAddress address, NetDeviceInfo info)
        {
            this.address = address;
            this.info = info;
            timestamp_dataReceived = 0;
            timeStamp_SyncSent = 0;
            timeStamp_SyncReceived = int.MaxValue;
        }

        public int GetOffsetAccuracy()
        {
            return timeStamp_SyncReceived - timeStamp_SyncSent; //Offset measure accuracy in ms
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct NetDeviceInfo
    {
        public byte deviceID;
        public byte HapticsAvalailable;
        public byte MoCapAvailable;  //Temporal
        public UInt32 batteryLevel;

        public NetDeviceInfo(byte deviceID, byte HapticInterfaceAvalailable, bool MoCapAvailable)
        {
            this.deviceID = deviceID;
            this.HapticsAvalailable = HapticInterfaceAvalailable;
            this.MoCapAvailable = Convert.ToByte(MoCapAvailable);
            batteryLevel = 0;
        }

        public NetDeviceInfo(byte[] data) : this()
        {
            int size = Marshal.SizeOf(this);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(data, 0, ptr, size);

            this = (NetDeviceInfo)Marshal.PtrToStructure(ptr, GetType());
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] GetBytes()
        {
            int size = Marshal.SizeOf(this);
            byte[] retVal = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, retVal, 0, size);
            Marshal.FreeHGlobal(ptr);
            return retVal;
        }
    };

    public NetClock GetNetClock()
    {
        return netClock;
    }

    public NetworkManager(int remoteNetComPort, int hostNetComPort) : base(remoteNetComPort, hostNetComPort)
    {
        netClock = new NetClock();
        netClock.Start();
    }
}
