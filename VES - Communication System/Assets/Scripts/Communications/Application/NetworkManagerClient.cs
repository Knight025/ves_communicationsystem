﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using UnityEngine;

public class NetworkManagerClient : NetworkManager
{
    /*Available remote devices. DeviceID-NetDevice entries*/
    private ConcurrentDictionary<int, NetDevice> netDevices;

    public NetworkManagerClient(int remoteNetComPort, int hostNetComPort) : base(remoteNetComPort, hostNetComPort)
    {
        netDevices = new ConcurrentDictionary<int, NetDevice>();
    }

    public void SyncNetClocks()
    {
        byte[] pkg;
        foreach (KeyValuePair<int, NetDevice> entry in netDevices)
        {
            NetDevice destination = entry.Value;
            if(entry.Value.GetOffsetAccuracy() > NetDevice.MIN_CLOCK_ACCURACY)
            {
                pkg = BuildCommandPkg((byte)NetworkCommand.SyncClock, BitConverter.GetBytes(netClock.GetTimeStamp()));
                defaultIO.SendPkgTo(destination.address, pkg);
                Debug.Log("(Client) Sync Pkg sent to " + destination.address.ToString() + " at " + netClock.GetTimeStamp() + ", accuracy: " + destination.GetOffsetAccuracy());
                destination.timeStamp_SyncSent = netClock.GetTimeStamp();
            }
        }
    }

    public void ScanNetDevices()
    {
        //Debug.Log("(Client) Scanning...");
        byte[] pkg = BuildCommandPkg((byte)NetworkCommand.GetNodeData);

        defaultIO.SendPkgTo(IPAddress.Broadcast, pkg);

        /*Workaround to connect to the smartphone which blocks broadcast packages. I configured it as AP, so I know its IP*/
        byte[] devAddress = defaultIO.hostIP.GetAddressBytes();
        devAddress[3] = 1;
        defaultIO.SendPkgTo(new IPAddress(devAddress), pkg);

        /*Workaround to connect to the static IP of the smartphone when using 'MiFibra-EAA9'*/
        defaultIO.SendPkgTo(new IPAddress(new byte[] { 192, 168, 1, 24 }), pkg);

        //for (byte i = 254; i > 1; i--)
        //{
        //    devAddress[3] = i;
        //    defaultIO.SendPkgTo(new IPAddress(devAddress), pkg);   //Temporal
        //}
    }

    /*Returns the info of the available devices*/
    public List<(IPAddress address, NetDeviceInfo info)> GetAvailableDevices()
    {
        List<(IPAddress, NetDeviceInfo)> retVal = new List<(IPAddress, NetDeviceInfo)>();

        foreach (KeyValuePair<int, NetDevice> entry in netDevices)
        {
            retVal.Add((entry.Value.address, entry.Value.info));
        }
        return retVal;
    }

    //public List<(IPAddress, NetDeviceInfo)> GetSynchronizedDevices()
    //{
    //    throw new NotImplementedException("I cannot detect yet if a device is synchronized or not");
    //    List<(IPAddress, NetDeviceInfo)> retVal = new List<(IPAddress, NetDeviceInfo)>();

    //    foreach (KeyValuePair<int, NetDevice> entry in netDevices)
    //    {
    //        if (entry.Value.isSynchronized)
    //            retVal.Add((entry.Value.address, entry.Value.info));
    //    }
    //    return retVal;
    //}

    /* Returns the ID of the available devices which answered with an 'devInfo' package within
     * the last 'timeSpan' miliseconds*/
    public List<(IPAddress address, NetDeviceInfo info)> GetAvailableDevices(int timeSpan)
    {
        List<(IPAddress, NetDeviceInfo)> retVal = new List<(IPAddress, NetDeviceInfo)>();
        int currentTime = netClock.GetTimeStamp();
        foreach (KeyValuePair<int, NetDevice> entry in netDevices)
        {
            if((currentTime - entry.Value.timestamp_dataReceived) < timeSpan)
                retVal.Add((entry.Value.address, entry.Value.info));
        }
        return retVal;
    }


    public override void ExecuteNetCommand(IPAddress origin, byte[] data)
    {
        //Debug.Log("(Client) Command " + data[0] +  " received from: " + origin.ToString());
        //TestVirtualScenario.stdOutput = "(Client) Command " + data[0] +  " received from: " + origin.ToString();
        NetworkCommand command = (NetworkCommand)GetPkgCommand(data);
        //defaultIO.SendPkgTo(new IPEndPoint(origin, 11001), BuildCommandPkg((int)NetworkCommand.GetNodeData));
        switch (command)
        {
            case NetworkCommand.AqSyncClock:
                foreach (KeyValuePair<int, NetDevice> entry in netDevices)
                {
                    NetDevice dev = entry.Value;
                    if (dev.address.Equals(origin))
                    {
                        dev.timeStamp_SyncReceived = netClock.GetTimeStamp();
                        
                        if (dev.GetOffsetAccuracy() > NetDevice.MIN_CLOCK_ACCURACY)
                        {
                            byte[] pkg = BuildCommandPkg((byte)NetworkCommand.SyncClock, BitConverter.GetBytes(netClock.GetTimeStamp()));
                            defaultIO.SendPkgTo(origin, pkg);
                            dev.timeStamp_SyncSent = netClock.GetTimeStamp();
                            Debug.Log("(Client) Sync Pkg from " + dev.address.ToString() + ". Offset accuracy: " + dev.GetOffsetAccuracy());
                        }
                        else
                        {
                            Debug.Log("(Client) AT LAST!! Sync Pkg from " + dev.address.ToString() + ". Offset accuracy: " + dev.GetOffsetAccuracy());
                        }
                        break;
                    }
                }
                break;
            case NetworkCommand.NewNodeData:
                NetDeviceInfo newDeviceInfo = new NetDeviceInfo(GetPkgData(data));
                //Debug.Log("(Network Manager Client) Device " + newDevice.info.deviceID + " (" + origin.ToString() +") with battery level: " + newDevice.info.batteryLevel);
                if (netDevices.TryGetValue(newDeviceInfo.deviceID, out NetDevice netDevice))
                {
                    netDevice.timestamp_dataReceived = netClock.GetTimeStamp();
                    netDevice.info = newDeviceInfo;
                }
                else
                {
                    NetDevice newDevice = new NetDevice(origin, newDeviceInfo);
                    newDevice.timestamp_dataReceived = netClock.GetTimeStamp();
                    if (!netDevices.TryAdd(newDeviceInfo.deviceID, newDevice)){
                        Debug.Log("(Client) Could not add new device");
                    }
                }

                break;
        }
    }
}
