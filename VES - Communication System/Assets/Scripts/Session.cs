﻿using System.Collections.Generic;
using System.Net;
using UnityEngine;
using static VirtualScenarioManager;

public class Session
{
    public static readonly int SCAN_PERIOD = 1000;
    public static readonly int STAY_ALIVE_TIMEOUT = SCAN_PERIOD * 2;

    public NetworkManagerClient networkClient;
    public VirtualScenarioManager virtualScenarioClient;
    public List<(byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager)> availableHaptics; /*Each haptic device has its own haptic channel*/
    public MoCapManager moCapManager;
    public byte moCapBodyID;
    public ChannelManager<BodyPart> mainMoCapChannel;
    public ChannelManager<BodyPart> pvasMoCapChannel;
    public ChannelManager<BodyPart> evasMoCapChannel;
    public GameObject listener;

    public List<(BodyPart.SpawnableElement prefabID, ProjectedVAS projectedVAS)> pvasDetectionAreas;
    public List<(BodyPart.SpawnableElement prefabID, EnvelopingVAS envelopingVAS)> evasDetectionAreas;

    public struct SessionInitData
    {
        public int netRemotePort;       /*Remote Port of "Network Manager"*/
        public int netHostPort;         /*Host Port of "Network Manager"*/
        public int vsRemotePort;        /*Remote Port of "Virtual Scenario Manager"*/
        public int vsHostPort;          /*Host Port of "Virtual Scenario Manager"*/
        public int moCapRemotePort;     /*Remote Port of "Motion Capture Manager"*/
        public int moCapHostPort;       /*Host Port of "Motion Capture Manager"*/
        public byte mainMocapChannel;   /*MoCap Channel for sharing data among all MoCap devices*/
        public byte pvasMocapChannel;   /*Local MoCap Channel which receives delayed data from 'mainMoCapChannel'. These data are used by 'PVAS'*/
        public byte evasMocapChannel;   /*Local MoCap Channel which receives delayed data from 'mainMoCapChannel'. These data are used by 'EVAS'*/
        public byte moCapBodyID;        /*ID of the body used in this session.*/
    }

    public Session(SessionInitData initData)
    {
        networkClient = new NetworkManagerClient(initData.netRemotePort, initData.netHostPort);
        networkClient.Init();
        virtualScenarioClient = new VirtualScenarioManager(initData.vsRemotePort, initData.vsHostPort, networkClient.GetNetClock());
        virtualScenarioClient.Init();

        moCapManager = new MoCapManager(initData.moCapRemotePort, initData.moCapHostPort, virtualScenarioClient, initData.mainMocapChannel, new JitterModeling.NoJitter());
        moCapManager.Init();
        mainMoCapChannel = moCapManager.GetChannel();
        pvasMoCapChannel = moCapManager.AddMulticast(initData.pvasMocapChannel); /*This is a copy of the user's movement with a different JitterModelling*/
        evasMoCapChannel = moCapManager.AddMulticast(initData.evasMocapChannel); /*The same. This allows for independent sound and haptic stimuli delay*/
        moCapBodyID = initData.moCapBodyID;

        availableHaptics = new List<(byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManagers)>();

        pvasDetectionAreas = new List<(BodyPart.SpawnableElement, ProjectedVAS)>();
        evasDetectionAreas = new List<(BodyPart.SpawnableElement prefabID, EnvelopingVAS envelopingVAS)>();

        listener = new GameObject("Listener");
        listener.AddComponent<AudioListener>();
    }

    public void Update()
    {
        virtualScenarioClient.UpdateHostScenario();
        virtualScenarioClient.UpdateRemoteScenario();
        moCapManager.UpdateBodyPose();
    }

    public void DeInit()
    {
        networkClient.DeInit();
        virtualScenarioClient.DeInit();
        moCapManager.DeInit();
    }

    public void ScanNetwork()
    {
        networkClient.ScanNetDevices();
    }
}
