﻿using UnityEngine;

public class GlassesClient : MonoBehaviour
{
    NetworkManagerServer networkManager;
    VirtualScenarioManager virtualScenarioManager;
    MoCapManager moCapManager;
    BodyPart head;
    BodyPart head2;

    public GameObject arCam;
    public readonly float MAX_POSITION_INCREMENT = 0.1f;
    public readonly float MAX_ROTATION_INCREMENT = 10;
    Vector3 prevPosition = Vector3.zero;
    Vector3 positionOffset = Vector3.zero;
    Quaternion prevRotation = Quaternion.identity;
    Quaternion rotationOffset = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        networkManager = new NetworkManagerServer(11001, 11000, new NetworkManager.NetDeviceInfo(6, 0, true));
        networkManager.Init();

        virtualScenarioManager = new VirtualScenarioManager(11003, 11002, networkManager.GetNetClock());
        virtualScenarioManager.Init();

        moCapManager = new MoCapManager(11005, 11004, virtualScenarioManager, 3, new JitterModeling.NoJitter());
        moCapManager.Init();

        head = moCapManager.AddBodyPart(BodyPart.SpawnableElement.Head, 1);
        head.updateType = BodyPart.UpdateType.PositionAndOrientation;

        head2 = moCapManager.AddBodyPart(BodyPart.SpawnableElement.Head, 2);
        head2.updateType = BodyPart.UpdateType.PositionAndOrientation;
    }


    // Update is called once per frame
    void Update()
    {
        if (arCam != null)
        {
            head.pose = JumpsFilter();
            head2.pose = new Pose(arCam.transform.position, arCam.transform.rotation);
        }
            
        virtualScenarioManager.UpdateHostScenario();
        virtualScenarioManager.UpdateRemoteScenario();
        moCapManager.UpdateBodyPose();
    }

    Pose JumpsFilter()
    {
        if((prevPosition - arCam.transform.position).magnitude > MAX_POSITION_INCREMENT)
        {
            positionOffset = prevPosition - arCam.transform.position;
        }
        if(Mathf.Abs(Quaternion.Angle(prevRotation, arCam.transform.rotation)) > MAX_ROTATION_INCREMENT)
        {
            rotationOffset = prevRotation * Quaternion.Inverse(arCam.transform.rotation);
        }

        return new Pose(arCam.transform.position + positionOffset, rotationOffset * arCam.transform.rotation);
    }

    public void OnDestroy()
    {
        networkManager.DeInit();
        virtualScenarioManager.DeInit();
    }
}
