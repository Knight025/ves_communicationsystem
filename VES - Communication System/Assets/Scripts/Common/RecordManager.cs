﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class RecordManager
{
    private bool isRecording;
    private bool highlighNextHostUpdate;
    private bool highlighNextRemoteUpdate;
    private ChannelRegister channelRegister;
    private JsonManager<ChannelRegister> jsonManager;

    [Serializable]
    public class ChannelRegister
    {
        public List<RegisterEntry> recordedRemoteData;
        public List<RegisterEntry> recordedHostData;

        public List<RegisterEntry> highlightedRemoteData;
        public List<RegisterEntry> highlightedHostData;

        public ChannelRegister()
        {
            recordedRemoteData = new List<RegisterEntry>();
            recordedHostData = new List<RegisterEntry>();
            highlightedRemoteData = new List<RegisterEntry>();
            highlightedHostData = new List<RegisterEntry>();
        }

        public void Clear()
        {
            recordedHostData.Clear();
            recordedRemoteData.Clear();
            highlightedRemoteData.Clear();
            highlightedHostData.Clear();
        }
    }

    [Serializable]
    public class RegisterEntry
    {
        public ulong ID;
        public byte[] data;
        public int triggerTimestamp;
        public int updateTimestamp;

        public RegisterEntry(ulong elementID, byte[] data, int triggerTimestamp, int updateTimestamp)
        {
            ID = elementID;
            this.data = data;
            this.triggerTimestamp = triggerTimestamp;
            this.updateTimestamp = updateTimestamp;
        }
    }

    public RecordManager(byte channel)
    {
        channelRegister = new ChannelRegister();
        jsonManager = new JsonManager<ChannelRegister>("Channel " + channel);
    }


    public void StartRecording()
    {
        isRecording = true;
        Debug.Log("Recording data...");
    }

    public void RecordRemoteUpdate(ulong elementID, byte[] update, int triggerTimestamp, int updateTimestamp)
    {
        if (isRecording)
        {
            channelRegister.recordedRemoteData.Add(new RegisterEntry(elementID, update, triggerTimestamp, updateTimestamp));
            if (highlighNextRemoteUpdate)
            {
                channelRegister.highlightedRemoteData.Add(new RegisterEntry(elementID, update, triggerTimestamp, updateTimestamp));
                highlighNextRemoteUpdate = false;
            }
        }
    }

    public void RecordHostUpdate(ulong elementID, byte[] update, int triggerTimestamp)
    {
        if (isRecording)
        {
            channelRegister.recordedHostData.Add(new RegisterEntry(elementID, update, triggerTimestamp, triggerTimestamp));
            if (highlighNextHostUpdate)
            {
                channelRegister.highlightedHostData.Add(new RegisterEntry(elementID, update, triggerTimestamp, triggerTimestamp));
                highlighNextRemoteUpdate = false;
            }
        }
    }

    public void HighlightNextUpdate()
    {
        highlighNextHostUpdate = true;
        highlighNextRemoteUpdate = true;
    }

    public void StopRecordingAndSave(string name)
    {
        if (!isRecording)
            return;
        string filePath = Application.persistentDataPath + "/RecordingData";

        if (!Directory.Exists(filePath))
            Directory.CreateDirectory(filePath);

        jsonManager.SetData(channelRegister);
        jsonManager.SaveDataToFile(name, filePath, false);
        isRecording = false;
        channelRegister.Clear();
        Debug.Log("Recording stopped. Data saved at: " + filePath);
    }
}
