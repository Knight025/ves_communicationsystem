﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using static VirtualScenarioManager;

public class EvasGUI : MonoBehaviour
{
    List<(BodyPart.SpawnableElement prefabID, EnvelopingVAS envelopingVAS)> detectionAreas;
    MoCapManager moCapManager;
    List<BodyPart> body;
    List<(byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager)> haptics;
    List<(byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager)> availableHaptics;
    private const string ANCHOR = "DetectionAreaAnchor";

    private InputField detectionDistance;
    private InputField spawnSpeed;
    private Toggle isActiveToggle;
    private Button acceptButton;
    private Button cancelButton;

    private Dropdown bodyPartDropdown;
    private Dropdown hapticDevicesDropdown;
    private Dropdown detectionAreasDropdown;
    private Button addEvastButton;
    private Button removeDetectionAreaButton;
    private Button exitButton;

    public void Init(List<(BodyPart.SpawnableElement, EnvelopingVAS envelopingVAS)> detectionAreas, MoCapManager moCapManager,
        List<(byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager)> haptics, byte bodyID, byte channel)
    {
        this.detectionAreas = detectionAreas;
        this.moCapManager = moCapManager;
        this.haptics = haptics;

        detectionDistance = transform.Find("Configuration/InputFields/DetectionDistanceMenu/InputField").GetComponent<InputField>();
        spawnSpeed = transform.Find("Configuration/InputFields/SpawnSpeedMenu/InputField").GetComponent<InputField>();
        isActiveToggle = transform.Find("Configuration/IsActiveToggle").GetComponent<Toggle>();
        acceptButton = transform.Find("Configuration/AcceptButton").GetComponent<Button>();
        cancelButton = transform.Find("Configuration/CancelButton").GetComponent<Button>();

        bodyPartDropdown = transform.Find("Manager/BodyPartDropdown").GetComponent<Dropdown>();
        hapticDevicesDropdown = transform.Find("Manager/HapticDevicesDropdown").GetComponent<Dropdown>();
        addEvastButton = transform.Find("Manager/AddPvasButton").GetComponent<Button>();
        detectionAreasDropdown = transform.Find("Configuration/DetectionAreaDropdown").GetComponent<Dropdown>();
        removeDetectionAreaButton = transform.Find("Configuration/RemovePvasButton").GetComponent<Button>();
        exitButton = transform.Find("ExitButton").GetComponent<Button>();

        addEvastButton.onClick.AddListener(OnClickAddDetectionArea);
        removeDetectionAreaButton.onClick.AddListener(OnClickRemoveDetectionArea);
        exitButton.onClick.AddListener(delegate { Destroy(gameObject); });
        acceptButton.onClick.AddListener(OnClickUpdateConfig);
        cancelButton.onClick.AddListener(RefreshDetectionAreaConfigGUI);


        /*Get all bodyParts which have a 'projectedVAS' anchor GameObject. If there are none, destroy this menu*/
        if (!moCapManager.TryGetBodyParts(bodyID, channel, out body, p => p.go.transform.Find(ANCHOR) != null))
        {
            Destroy(gameObject);
            return;
        }

        /*Update the 'parentDropdown' with compatible bodyParts*/
        List<string> bodyNames = new List<string>();
        foreach (BodyPart bodyPart in body)
        {
            bodyNames.Add(Enum.GetName(typeof(BodyPart.SpawnableElement), bodyPart.prefabID));
        }
        bodyPartDropdown.ClearOptions();
        bodyPartDropdown.AddOptions(bodyNames);

        /* WARNING! For now, the HUZZAH32 devices with a haptic actuator only supports one remote haptic stimuli element in the channel.
        * If there are more than one haptic stimuli, the haptic interface will overwrite continuously the corresponding ERM driver signals.
        * Therefore, the only truly available haptic channels are those with no haptic stimuli.*/
        availableHaptics = haptics.FindAll(p => p.channelManager.HostElementsCount == 0);

        List<string> hapticDevicesNames = new List<string>();
        Debug.Log("Number of available devices: " + haptics.Count);
        foreach ((byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManagers) haptic in availableHaptics)
        {
            hapticDevicesNames.Add(haptic.deviceID.ToString());
        }
        hapticDevicesDropdown.ClearOptions();
        hapticDevicesDropdown.AddOptions(hapticDevicesNames);

        detectionAreasDropdown.onValueChanged.AddListener(delegate { RefreshDetectionAreaConfigGUI(); });

        RefreshDetectionAreaGUI();
        RefreshHapticDevicesGUI();
    }


    private void OnClickAddDetectionArea()
    {
        Transform newParent;
        /*Find the anchor withim the 'bodyPart' prefab, and set it as 'parent'*/
        if ((newParent = body[bodyPartDropdown.value].go.transform.Find(ANCHOR)) != null)
        {
            ChannelManager<HapticStimuli> currentChannelManager = availableHaptics[hapticDevicesDropdown.value].channelManager;
            IPAddress currentDeviceAddress = availableHaptics[hapticDevicesDropdown.value].address;
            EnvelopingVAS newEnvelopingVAS = new GameObject("EnvelopingVAS").AddComponent<EnvelopingVAS>();

            currentChannelManager.SubscribeNewDevice(currentDeviceAddress);
            newEnvelopingVAS.Init(currentChannelManager);
            newEnvelopingVAS.AdoptMe(newParent);
            newEnvelopingVAS.SetEnable(true);
            detectionAreas.Add(((BodyPart.SpawnableElement)body[bodyPartDropdown.value].prefabID, newEnvelopingVAS));
        }
        RefreshHapticDevicesGUI();
        RefreshDetectionAreaGUI();        
    }

    private void OnClickRemoveDetectionArea()
    {
        if (detectionAreasDropdown.options.Count == 0)
            return;

        EnvelopingVAS currentEnvelopingVAS = detectionAreas[detectionAreasDropdown.value].envelopingVAS;

        currentEnvelopingVAS.GetChannelManager().UnsubscribeAllDevices();
        currentEnvelopingVAS.SetEnable(false);
        currentEnvelopingVAS.DeInit();

        detectionAreas.RemoveAt(detectionAreasDropdown.value);

        RefreshHapticDevicesGUI();
        RefreshDetectionAreaGUI();
    }

    private void SetDetAreaConfigInteractable(bool enable)
    {
        detectionAreasDropdown.interactable = enable;
        removeDetectionAreaButton.interactable = enable;
        detectionDistance.interactable = enable;
        spawnSpeed.interactable = enable;
        isActiveToggle.interactable = enable;

        acceptButton.interactable = enable;
        cancelButton.interactable = enable;
    }

    private void RefreshDetectionAreaGUI()
    {
        if (detectionAreas.Count == 0)
        {
            SetDetAreaConfigInteractable(false);
            return;
        }

        List<string> options = new List<string>();
        foreach ((BodyPart.SpawnableElement prefabID, EnvelopingVAS envelopingVAS) entry in detectionAreas)
        {
            options.Add(detectionAreas.IndexOf(entry) + "-" + Enum.GetName(typeof(BodyPart.SpawnableElement), entry.prefabID));
        }
        detectionAreasDropdown.ClearOptions();
        detectionAreasDropdown.AddOptions(options);
        RefreshDetectionAreaConfigGUI();
        SetDetAreaConfigInteractable(true);
    }

    private void RefreshHapticDevicesGUI()
    {
        availableHaptics = haptics.FindAll(p => p.channelManager.HostElementsCount == 0);
        List<string> hapticDevicesNames = new List<string>();
        foreach ((byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManagers) haptic in availableHaptics)
        {
            hapticDevicesNames.Add(haptic.deviceID.ToString());
        }
        hapticDevicesDropdown.ClearOptions();
        hapticDevicesDropdown.AddOptions(hapticDevicesNames);
    }

    private void RefreshDetectionAreaConfigGUI()
    {
        EnvelopingVAS envelopingVAS = detectionAreas[detectionAreasDropdown.value].envelopingVAS;
        EnvelopingVASConfig config = envelopingVAS.GetConfig();

        detectionDistance.text = config.detectionDistance.ToString();
        spawnSpeed.text = config.period.ToString();

        isActiveToggle.isOn = envelopingVAS.IsTestEnabled();
    }

    private void OnClickUpdateConfig()
    {
        EnvelopingVAS envelopingVAS = detectionAreas[detectionAreasDropdown.value].envelopingVAS;
        EnvelopingVASConfig config = new EnvelopingVASConfig();

        config.detectionDistance = float.Parse(detectionDistance.text);
        config.period = float.Parse(spawnSpeed.text);

        envelopingVAS.SetConfig(config);
        envelopingVAS.SetEnable(isActiveToggle.isOn);
    }
}
