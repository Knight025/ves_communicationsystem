﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class DevicesGUI : MonoBehaviour
{
    private NetworkManagerClient networkManager;
    public static Dictionary<int, (RawImage, Text)> iconList;


    // Start is called before the first frame update
    public void Init(NetworkManagerClient networkManager)
    {
        this.networkManager = networkManager;

        iconList = new Dictionary<int, (RawImage, Text)>();

        iconList.Add(1, (transform.Find("Huzzah1/Icon").GetComponent<RawImage>(), GameObject.Find("Huzzah1/Text").GetComponent<Text>()));
        iconList.Add(2, (transform.Find("Huzzah2/Icon").GetComponent<RawImage>(), GameObject.Find("Huzzah2/Text").GetComponent<Text>()));
        iconList.Add(3, (transform.Find("Huzzah3/Icon").GetComponent<RawImage>(), GameObject.Find("Huzzah3/Text").GetComponent<Text>()));
        iconList.Add(4, (transform.Find("Huzzah4/Icon").GetComponent<RawImage>(), GameObject.Find("Huzzah4/Text").GetComponent<Text>()));
        iconList.Add(5, (transform.Find("Huzzah5/Icon").GetComponent<RawImage>(), GameObject.Find("Huzzah5/Text").GetComponent<Text>()));
        iconList.Add(6, (transform.Find("Glasses/Icon").GetComponent<RawImage>(), GameObject.Find("Glasses/Text").GetComponent<Text>()));

        InvokeRepeating("UpdateDevicesPanel", 1, Session.SCAN_PERIOD/1000f);
    }

    void UpdateDevicesPanel()
    {
        //networkManager.SyncNetClocks(null);

        /*The network is scanned periodically in 'Main.cs'*/
        List<(IPAddress address, NetworkManager.NetDeviceInfo info)> connectedDevices = networkManager.GetAvailableDevices(Session.STAY_ALIVE_TIMEOUT);

        foreach (int deviceID in iconList.Keys)
        {
            iconList.TryGetValue(deviceID, out (RawImage icon, Text deviceInfo) entry);
            if (connectedDevices.Exists(p => p.info.deviceID == deviceID))
            {
                (IPAddress address, NetworkManager.NetDeviceInfo info) dev = connectedDevices.Find(p => p.info.deviceID == deviceID);
                entry.icon.color = Color.green;
                //entry.deviceInfo.text = "Battery: " + dev.info.batteryLevel + ".   IP: " + dev.address.ToString() + ".   Haptics: " + (dev.info.HapticsAvalailable == 1 ? "ON" : "OFF");
                entry.deviceInfo.text = dev.info.batteryLevel.ToString();
                entry.deviceInfo.text += "\t\t\t" + (dev.info.HapticsAvalailable > 0 ? "ON " : "OFF");
                entry.deviceInfo.text += "\t\t\t\t" + dev.address.ToString();
            }
            else
            {
                entry.icon.color = Color.red;
                entry.deviceInfo.text = "Offline";
            }
        }
    }
}
