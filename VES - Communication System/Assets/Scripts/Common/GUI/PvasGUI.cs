﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PvasGUI : MonoBehaviour
{
    //ProjectedVAS projectedVAS;
    List<(BodyPart.SpawnableElement prefabID, ProjectedVAS projectedVAS)> detectionAreas;
    MoCapManager moCapManager;
    List<BodyPart> body;
    private const string ANCHOR = "DetectionAreaAnchor";

    //GUI elements
    private Dropdown pixelMatrix_M;
    private Dropdown pixelMatrix_N;
    private InputField fieldOfView_X;
    private InputField fieldOfView_Y;
    private InputField detectionDistance;
    private InputField spawnSpeed;
    private InputField randomizerInt;
    private Toggle isActiveToggle;
    private Button acceptButton;
    private Button cancelButton;

    private Dropdown bodyPartDropdown;
    private Dropdown detectionAreasDropdown;
    private Button addPvastButton;
    private Button removeDetectionAreaButton;
    private Button exitButton;

    public void Init(List<(BodyPart.SpawnableElement, ProjectedVAS)> projectedVASList, MoCapManager moCapManager, byte bodyID, byte channel)
    {
        this.detectionAreas = projectedVASList;
        this.moCapManager = moCapManager;

        pixelMatrix_N = transform.Find("Configuration/InputFields/MatrixMenu/NDropdown").GetComponent<Dropdown>();
        pixelMatrix_M = transform.Find("Configuration/InputFields/MatrixMenu/MDropdown").GetComponent<Dropdown>();
        fieldOfView_X = transform.Find("Configuration/InputFields/FieldOfViewMenu/AngleXInputField").GetComponent<InputField>();
        fieldOfView_Y = transform.Find("Configuration/InputFields/FieldOfViewMenu/AngleYInputField").GetComponent<InputField>();
        detectionDistance = transform.Find("Configuration/InputFields/DetectionDistanceMenu/InputField").GetComponent<InputField>();
        spawnSpeed = transform.Find("Configuration/InputFields/SpawnSpeedMenu/InputField").GetComponent<InputField>();
        randomizerInt = transform.Find("Configuration/InputFields/RandomizerMenu/InputField").GetComponent<InputField>();
        isActiveToggle = transform.Find("Configuration/IsActiveToggle").GetComponent<Toggle>();
        acceptButton = transform.Find("Configuration/AcceptButton").GetComponent<Button>();
        cancelButton = transform.Find("Configuration/CancelButton").GetComponent<Button>();

        bodyPartDropdown = transform.Find("Manager/BodyPartDropdown").GetComponent<Dropdown>();
        addPvastButton = transform.Find("Manager/AddPvasButton").GetComponent<Button>();
        detectionAreasDropdown = transform.Find("Configuration/DetectionAreaDropdown").GetComponent<Dropdown>();
        removeDetectionAreaButton = transform.Find("Configuration/RemovePvasButton").GetComponent<Button>();
        exitButton = transform.Find("ExitButton").GetComponent<Button>();

        addPvastButton.onClick.AddListener(OnClickAddDetectionArea);
        removeDetectionAreaButton.onClick.AddListener(OnClickRemoveDetectionArea);
        exitButton.onClick.AddListener(delegate { Destroy(gameObject); });
        acceptButton.onClick.AddListener(OnClickUpdateConfig);
        cancelButton.onClick.AddListener(delegate{ RefreshConfig(detectionAreas[detectionAreasDropdown.value].projectedVAS); });
        

        /*Get all bodyParts which have a 'projectedVAS' anchor GameObject. If there are none, destroy this menu*/
        if (!moCapManager.TryGetBodyParts(bodyID, channel, out body, p => p.go.transform.Find(ANCHOR) != null))
        {
            Destroy(gameObject);
            return;
        }

        /*Update the 'parentDropdown' with compatible bodyParts*/
        List<string> bodyNames = new List<string>();
        foreach (BodyPart bodyPart in body)
        {
            bodyNames.Add(Enum.GetName(typeof(BodyPart.SpawnableElement), bodyPart.prefabID));
        }
        bodyPartDropdown.ClearOptions();
        bodyPartDropdown.AddOptions(bodyNames);

        detectionAreasDropdown.onValueChanged.AddListener(delegate { RefreshConfig(detectionAreas[detectionAreasDropdown.value].projectedVAS); });

        RefreshDetectionAreas();
    }

    private void OnClickAddDetectionArea()
    {
        Transform newParent;
        /*Find the anchor withim the 'bodyPart' prefab, and set it as 'parent'*/
        if((newParent = body[bodyPartDropdown.value].go.transform.Find(ANCHOR)) != null)
        {
            ProjectedVAS projectedVAS = new GameObject("ProjectedVAS").AddComponent<ProjectedVAS>();
            projectedVAS.Init();
            projectedVAS.AdoptMe(newParent);
            projectedVAS.EnableTest(true);
            detectionAreas.Add(((BodyPart.SpawnableElement)body[bodyPartDropdown.value].prefabID, projectedVAS));
        }
        RefreshDetectionAreas();
        SetInteractableConfig(true);
    }

    private void OnClickRemoveDetectionArea()
    {
        if (detectionAreasDropdown.options.Count == 0)
            return;

        Destroy(detectionAreas[detectionAreasDropdown.value].projectedVAS.gameObject);
        
        detectionAreas.Remove(detectionAreas[detectionAreasDropdown.value]);
        RefreshDetectionAreas();
        if(detectionAreas.Count == 0)
        {
            SetInteractableConfig(false);
        }
    }

    private void SetInteractableConfig(bool enable)
    {
        detectionAreasDropdown.interactable = enable;
        removeDetectionAreaButton.interactable = enable;
        pixelMatrix_M.interactable = enable;
        pixelMatrix_N.interactable = enable;
        fieldOfView_X.interactable = enable;
        fieldOfView_Y.interactable = enable;
        detectionDistance.interactable = enable;
        spawnSpeed.interactable = enable;
        randomizerInt.interactable = enable;
        isActiveToggle.interactable = enable;

        acceptButton.interactable = enable;
        cancelButton.interactable = enable;
    }


    private void RefreshDetectionAreas()
    {
        detectionAreasDropdown.ClearOptions();
        if (detectionAreas.Count == 0)
        {
            SetInteractableConfig(false);
            return;
        }

        List<string> options = new List<string>();
        foreach((BodyPart.SpawnableElement prefabID, ProjectedVAS projectedVAS) entry in detectionAreas)
        {
            options.Add(detectionAreas.IndexOf(entry) + "-" + Enum.GetName(typeof(BodyPart.SpawnableElement), entry.prefabID));
        }
        detectionAreasDropdown.AddOptions(options);
        RefreshConfig(detectionAreas[detectionAreasDropdown.value].projectedVAS);
    }

    private void RefreshConfig(ProjectedVAS projectedVAS)
    {
        //ProjectedVAS projectedVAS = detectionAreas[detectionAreasDropdown.value].projectedVAS;
        ProjectedVASConfig config = projectedVAS.GetConfig();

        pixelMatrix_M.value = pixelMatrix_M.options.FindIndex(p => int.Parse(p.text) == config.M);
        pixelMatrix_N.value = pixelMatrix_N.options.FindIndex(p => int.Parse(p.text) == config.N);
        fieldOfView_X.text = config.fieldOfViewX.ToString();
        fieldOfView_Y.text = config.fieldOfViewY.ToString();
        detectionDistance.text = config.detectionDistance.ToString();
        spawnSpeed.text = config.period.ToString();
        randomizerInt.text = config.randomizer.ToString();

        isActiveToggle.isOn = projectedVAS.IsTestEnabled();
    }

    private void OnClickUpdateConfig()
    {
        ProjectedVAS projectedVAS = detectionAreas[detectionAreasDropdown.value].projectedVAS;
        ProjectedVASConfig config = new ProjectedVASConfig();

        config.M = int.Parse(pixelMatrix_M.options[pixelMatrix_M.value].text);
        config.N = int.Parse(pixelMatrix_N.options[pixelMatrix_N.value].text);
        config.fieldOfViewX = float.Parse(fieldOfView_X.text);
        config.fieldOfViewY = float.Parse(fieldOfView_Y.text);
        config.detectionDistance = float.Parse(detectionDistance.text);
        config.period = float.Parse(spawnSpeed.text);
        config.randomizer = int.Parse(randomizerInt.text);

        projectedVAS.SetConfig(config);
        projectedVAS.EnableTest(isActiveToggle.isOn);
    }
}
