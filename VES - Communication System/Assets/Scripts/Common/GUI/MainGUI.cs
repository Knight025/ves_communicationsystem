﻿using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static NetworkManager;
using static VirtualScenarioManager;

public class MainGUI : MonoBehaviour
{
    private Session session;
    private Camera userCamera;

    private Button syncButton;
    private Button subscribeButton;
    private Button unsubscribeButton;
    private Button deleteRemoteButton;
    private Button calibratePoseButton;
    private Button calibrateIMUOffsetButton;
    private Button resetPositionAndScaleButton;
    private Button switchCameraButton;
    private InputField userHeightInputField;
    private Button pvasConfigButton;
    private Button evasConfigButton;
    private Button jitterConfigButton;
    private Dropdown scenariosDropdown;
    private Button startRecordingButton;
    private Button highlightRecordingButton;
    private Button stopRecordingButton;
    private InputField fileNameInputField;

    private static GameObject scenarioLoaded = null;

    private static Object[] scenarios;

    private List<Vector3> handReference;
    //private JsonManager<List<Pose>> handReference;

    public void Init(Session session)
    {
        this.session = session;

        syncButton = transform.Find("Network/SyncButton").GetComponent<Button>();
        subscribeButton = transform.Find("VirtualScenario/SubscribeButton").GetComponent<Button>();
        unsubscribeButton = transform.Find("VirtualScenario/UnsubscribeButton").GetComponent<Button>();
        deleteRemoteButton = transform.Find("VirtualScenario/DeleteRemoteButton").GetComponent<Button>();
        calibratePoseButton = transform.Find("MoCap/CalibrateMoCapButton").GetComponent<Button>();
        calibrateIMUOffsetButton = transform.Find("MoCap/CalibrateIMUOffsetButton").GetComponent<Button>();
        resetPositionAndScaleButton = transform.Find("MoCap/ResetOriginButton").GetComponent<Button>();
        switchCameraButton = transform.Find("MoCap/SwitchCameraButton").GetComponent<Button>();
        userHeightInputField = transform.Find("MoCap/UserHeightInputField").GetComponent<InputField>();
        pvasConfigButton = transform.Find("TestsMenu/PvasConfigurationButton").GetComponent<Button>();
        evasConfigButton = transform.Find("TestsMenu/EvasConfigurationButton").GetComponent<Button>();
        jitterConfigButton = transform.Find("TestsMenu/JitterConfigurationButton").GetComponent<Button>();
        startRecordingButton = transform.Find("Recording/StartRecordingButton").GetComponent<Button>();
        highlightRecordingButton = transform.Find("Recording/HighlightNextRecordingButton").GetComponent<Button>();
        stopRecordingButton = transform.Find("Recording/StopRecordingButton").GetComponent<Button>();
        fileNameInputField = transform.Find("Recording/FileNameInputField").GetComponent<InputField>();
        userCamera = transform.Find("UserCamera").GetComponent<Camera>();

        syncButton.onClick.AddListener(OnClickSyncNetClock);
        subscribeButton.onClick.AddListener(OnClickSubscribe);
        unsubscribeButton.onClick.AddListener(OnClickUnsubscribe);
        deleteRemoteButton.onClick.AddListener(OnClickDeleteRemote);
        calibratePoseButton.onClick.AddListener(OnClickCalibrateMoCapPose);
        calibrateIMUOffsetButton.onClick.AddListener(delegate { LoadDoubleCheckMenu(OnClickCalibrateIMUOffset); });
        pvasConfigButton.onClick.AddListener(OnClickLoadPvasConfig);
        evasConfigButton.onClick.AddListener(OnClickLoadEvasConfig);
        resetPositionAndScaleButton.onClick.AddListener(OnClickResetPositionAndScale);
        jitterConfigButton.onClick.AddListener(OnClickLoadJitterConfig);
        startRecordingButton.onClick.AddListener(OnClickStartRecording);
        highlightRecordingButton.onClick.AddListener(OnClickHighlightNextRecording);
        stopRecordingButton.onClick.AddListener(OnClickStopRecording);
        switchCameraButton.onClick.AddListener(OnClickSwitchUserCamera);

        userCamera.enabled = false;

        /*Load the scenarios' dropdown*/
        scenariosDropdown = GameObject.Find("Environment/ScenesDropdown").GetComponent<Dropdown>();
        List<string> scenarioNames;
        (scenarioNames, scenarios) = LoadObjects("Prefabs/Scenarios");
        scenariosDropdown.ClearOptions();
        scenariosDropdown.AddOptions(scenarioNames);
        scenariosDropdown.onValueChanged.AddListener(delegate { UpdateScenarioDropdown(scenariosDropdown.value); });

        /*Load the default scenario 'Construct'*/
        int defaultScenarioIndex = scenarioNames.FindIndex(p => p.Equals("Construct"));
        if (defaultScenarioIndex >= 0)
            UpdateScenarioDropdown(defaultScenarioIndex);

        deleteRemoteButton.interactable = false;
        highlightRecordingButton.interactable = false;
        stopRecordingButton.interactable = false;

        /*Instantiate and init the 'devicesPanel'*/
        Instantiate((GameObject)Resources.Load("Prefabs/GUI/DevicesPanel"), transform.parent).GetComponent<DevicesGUI>().Init(session.networkClient);

        handReference = new List<Vector3>();
    }

    private static void UpdateScenarioDropdown(int value)
    {
        if (scenarioLoaded != null)
            GameObject.Destroy(scenarioLoaded);
        scenarioLoaded = GameObject.Instantiate((GameObject)scenarios[value]);
        scenarioLoaded.transform.Find("Camera").gameObject.SetActive(true);
    }

    public static (List<string>, Object[]) LoadObjects(string filePath)
    {
        List<string> names = new List<string>();
        Object[] objects = Resources.LoadAll(filePath);

        foreach (Object obj in objects) { names.Add(obj.name); }

        return (names, objects);
    }

    public void OnClickSubscribe()
    {
        List<(IPAddress, NetworkManager.NetDeviceInfo)> list = session.networkClient.GetAvailableDevices();
        Debug.Log("(Client) Subscribing...");
        foreach ((IPAddress devAddress, NetworkManager.NetDeviceInfo devInfo) in list)
        {
            session.mainMoCapChannel.SubscribeTo(devAddress);
        }
    }

    public void OnClickUnsubscribe()
    {
        List<(IPAddress, NetworkManager.NetDeviceInfo)> list = session.networkClient.GetAvailableDevices();

        Debug.Log("(Client) Unsubscribing...");
        foreach ((IPAddress devAddress, NetworkManager.NetDeviceInfo devInfo) in list)
        {
            session.mainMoCapChannel.UnsubscribeFrom(devAddress);
        }
    }

    public void OnClickDeleteRemote()
    {
        List<(IPAddress, NetworkManager.NetDeviceInfo)> list = session.networkClient.GetAvailableDevices();

        foreach ((IPAddress devAddress, NetworkManager.NetDeviceInfo devInfo) in list)
        {
            /*ERROR: THIS SEPARATES THE BODY PARTS*/
            session.mainMoCapChannel.DestroyRemoteScenarioElements(devAddress);
        }
    }

    public void OnClickSyncNetClock()
    {
        session.networkClient.SyncNetClocks();
    }

    public void OnClickCalibrateMoCapPose()
    {
        session.moCapManager.CalibrateBody(1);

        
        if (session.moCapManager.TryGetBodyParts(1, out List<BodyPart> body))
        {
            /*Attach the audioListener to the head*/
            BodyPart head;
            if ((head = body.Find(p => p.prefabID == (byte)BodyPart.SpawnableElement.Head)) != null)
            {
                session.listener.transform.parent = head.go.transform;
                session.listener.transform.localPosition = Vector3.zero;
                session.listener.transform.localRotation = Quaternion.identity;
            }
        }
    }

    public void OnClickLoadPvasConfig()
    {
        GameObject pvasGUI = Instantiate((GameObject)Resources.Load("Prefabs/GUI/PVASPanel"), transform.parent);
        pvasGUI.GetComponent<PvasGUI>().Init(session.pvasDetectionAreas, session.moCapManager, session.moCapBodyID, session.pvasMoCapChannel.channel);
    }

    public void OnClickLoadEvasConfig()
    {
        List<(IPAddress, NetDeviceInfo)> availableHapticDevices = session.networkClient.GetAvailableDevices().FindAll(p => p.info.HapticsAvalailable > 0);
        foreach((IPAddress address, NetDeviceInfo info) device in availableHapticDevices)
        {
            if(session.virtualScenarioClient.AddChannel(device.info.HapticsAvalailable, out ChannelManager<HapticStimuli> hapChannelManager))
            {
                session.availableHaptics.Add((device.info.deviceID, device.address, hapChannelManager));
            }
        }

        GameObject evasGUI = Instantiate((GameObject)Resources.Load("Prefabs/GUI/EVASPanel"), transform.parent);
        Debug.Log("Session available haptics: " + session.availableHaptics.Count);
        evasGUI.GetComponent<EvasGUI>().Init(session.evasDetectionAreas, session.moCapManager, session.availableHaptics, session.moCapBodyID, session.evasMoCapChannel.channel);
    }

    public void OnClickLoadJitterConfig()
    {
        GameObject jitterGUI = Instantiate((GameObject)Resources.Load("Prefabs/GUI/JitterPanel"), transform.parent);
        jitterGUI.GetComponent<JitterGUI>().Init(session.pvasMoCapChannel.GetJitterManager(), session.evasMoCapChannel.GetJitterManager());
    }

    public void OnClickCalibrateIMUOffset()
    {
        List<(IPAddress, NetworkManager.NetDeviceInfo)> list = session.networkClient.GetAvailableDevices();
        
        foreach ((IPAddress devAddress, NetworkManager.NetDeviceInfo devInfo) in list)
        {
            session.moCapManager.CalibrateIMUOffset(devAddress);
        }
    }

    public void OnClickResetPositionAndScale()
    {
        if (scenarioLoaded == null)
            return;
        float userHeight = 1.7f;
        try { userHeight = float.Parse(userHeightInputField.text); } catch { }

        List<BodyPart> body;
        BodyPart head;
        if (session.moCapManager.TryGetBodyParts(1, out body) && (head = body.Find(p => p.prefabID == (byte)BodyPart.SpawnableElement.Head)) != null)
        {
            scenarioLoaded.transform.position = head.go.transform.position - Vector3.up*userHeight; /*The user's height should be measured in runtime*/
            Vector3 userForward = head.go.transform.forward;
            userForward.y = 0;
            scenarioLoaded.transform.rotation *= Quaternion.FromToRotation(scenarioLoaded.transform.forward, userForward);
            scenarioLoaded.transform.localScale = new Vector3(1.7f/ userHeight, 1.7f / userHeight, 1.7f / userHeight);
        }
    }

    public void OnClickStartRecording()
    {
        session.mainMoCapChannel.GetRecordManager().StartRecording();
        session.pvasMoCapChannel.GetRecordManager().StartRecording();
        session.evasMoCapChannel.GetRecordManager().StartRecording();
        foreach((byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager) entry in session.availableHaptics)
        {
            entry.channelManager.GetRecordManager().StartRecording();
        }
        startRecordingButton.interactable = false;
        highlightRecordingButton.interactable = true;
        stopRecordingButton.interactable = true;
    }

    public void OnClickHighlightNextRecording()
    {
        session.mainMoCapChannel.GetRecordManager().HighlightNextUpdate();
        session.pvasMoCapChannel.GetRecordManager().HighlightNextUpdate();
        session.evasMoCapChannel.GetRecordManager().HighlightNextUpdate();
        foreach ((byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager) entry in session.availableHaptics)
        {
            entry.channelManager.GetRecordManager().HighlightNextUpdate();
        }

        /*Save the position of the hand*/
        List<BodyPart> body;
        BodyPart rightForearm;
        if (session.moCapManager.TryGetBodyParts(1, out body) && (rightForearm = body.Find(p => p.prefabID == (byte)BodyPart.SpawnableElement.RightForearm)) != null)
        {
            handReference.Add(rightForearm.go.transform.Find("HandReference").position);
        }
    }

    public void OnClickStopRecording()
    {
        session.mainMoCapChannel.GetRecordManager().StopRecordingAndSave(fileNameInputField.text + "_" + scenarioLoaded.name);
        session.pvasMoCapChannel.GetRecordManager().StopRecordingAndSave(fileNameInputField.text + "_" + scenarioLoaded.name);
        session.evasMoCapChannel.GetRecordManager().StopRecordingAndSave(fileNameInputField.text + "_" + scenarioLoaded.name);
        foreach ((byte deviceID, IPAddress address, ChannelManager<HapticStimuli> channelManager) entry in session.availableHaptics)
        {
            entry.channelManager.GetRecordManager().StopRecordingAndSave(fileNameInputField.text + "_" + scenarioLoaded.name);
        }
        /*Save the position of the hand in a .json file*/
        JsonManager<List<Vector3>> aux = new JsonManager<List<Vector3>>("handReference", handReference);
        string filePath = Application.persistentDataPath + "/RecordingData";
        aux.SaveDataToFile(fileNameInputField.text + "_" + scenarioLoaded.name, filePath, false);
        handReference.Clear();
        
        startRecordingButton.interactable = true;
        highlightRecordingButton.interactable = false;
        stopRecordingButton.interactable = false;
    }

    public void OnClickSwitchUserCamera()
    {
        if (userCamera.enabled == true)
        {
            userCamera.enabled = false;
            return;
        }

        List<BodyPart> body; 
        BodyPart torso;
        if (session.moCapManager.TryGetBodyParts(1, out body) && (torso = body.Find(p => p.prefabID == (byte)BodyPart.SpawnableElement.Torso)) != null)
        {
            userCamera.transform.SetParent(torso.go.transform.Find("CameraAnchor"), false);
            userCamera.enabled = true;
        }
    }

    /*If the user confirms the action through the GUI, 'call' will be executed.*/
    public void LoadDoubleCheckMenu(UnityAction call)
    {
        GameObject doubleCheckMenu = Instantiate((GameObject)Resources.Load("Prefabs/GUI/DoubleCheckPanel"), transform.parent);
        doubleCheckMenu.GetComponent<DoubleCheckGUI>().Init(call);
    }
}
