﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JitterGUI : MonoBehaviour
{
    JitterManager pvasJitterManager;
    JitterManager evasJitterManager;

    private Toggle pvasJitterToggle;
    private InputField pvasLambdaInputField;
    private Toggle evasJitterToggle;
    private InputField evasLambdaInputField;

    private Button acceptButton;
    private Button cancelButton;

    private const int MAX_PKG_OUTPUT_PER_CALL = 20;

    public void Init(JitterManager pvasJitterManager, JitterManager evasJitterManager)
    {
        this.pvasJitterManager = pvasJitterManager;
        this.evasJitterManager = evasJitterManager;

        pvasJitterToggle = transform.Find("PVASConfiguration/IsActiveToggle").GetComponent<Toggle>();
        pvasLambdaInputField = transform.Find("PVASConfiguration/InputFields/LambdaMenu/InputField").GetComponent<InputField>();
        evasJitterToggle = transform.Find("EVASConfiguration/IsActiveToggle").GetComponent<Toggle>();
        evasLambdaInputField = transform.Find("EVASConfiguration/InputFields/LambdaMenu/InputField").GetComponent<InputField>();
        acceptButton = transform.Find("AcceptButton").GetComponent<Button>();
        cancelButton = transform.Find("CancelButton").GetComponent<Button>();

        acceptButton.onClick.AddListener(delegate { if (OnClickUpdateConfig()) Destroy(gameObject); });
        cancelButton.onClick.AddListener(delegate { Destroy(gameObject); });

        RefreshConfig();
    }

    public void RefreshConfig()
    {
        JitterModeling pvasJitterModeling = pvasJitterManager.GetJitterModeling();
        JitterModeling evasJitterModeling = evasJitterManager.GetJitterModeling();

        if (pvasJitterModeling.GetType().Equals(typeof(JitterModeling.PoissonDistribution)))
        {
            pvasLambdaInputField.text = "" + ((JitterModeling.PoissonDistribution)pvasJitterModeling).lambda;
            pvasJitterToggle.isOn = true;
        }
        else
        {
            pvasLambdaInputField.text = "1,2";
            pvasJitterToggle.isOn = false;
        }
        
        if (evasJitterModeling.GetType().Equals(typeof(JitterModeling.PoissonDistribution)))
        {
            evasLambdaInputField.text = "" + ((JitterModeling.PoissonDistribution)evasJitterModeling).lambda;
            evasJitterToggle.isOn = true;
        }
        else
        {
            evasLambdaInputField.text = "1,2";
            evasJitterToggle.isOn = false;
        }
    }

    public bool OnClickUpdateConfig()
    {
        try
        {
            float pvasLambda = float.Parse(pvasLambdaInputField.text);
            float evasLambda = float.Parse(evasLambdaInputField.text);
            pvasJitterManager.SetJitterModelling(pvasJitterToggle.isOn ?
                (JitterModeling)new JitterModeling.PoissonDistribution(pvasLambda, MAX_PKG_OUTPUT_PER_CALL) :
                new JitterModeling.NoJitter());
            evasJitterManager.SetJitterModelling(evasJitterToggle.isOn ?
                (JitterModeling)new JitterModeling.PoissonDistribution(evasLambda, MAX_PKG_OUTPUT_PER_CALL) :
                new JitterModeling.NoJitter());
            return true;
        }
        catch
        {
            RefreshConfig();
            return false;
        }
    }
}
