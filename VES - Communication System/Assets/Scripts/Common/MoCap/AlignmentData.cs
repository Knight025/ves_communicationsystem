﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignmentData : MonoBehaviour
{
    /*Pose calibration data*/
    public BodyPart.SpawnableElement pivotPrefab;   /*This refers to the anchor prefab*/
    public string pivotName; /*This refers to the children gameobject of 'pivotPrefab' which serves as the pivot point*/

    /*IMU Calibration data*/
    public Vector3 initialRotationEuler; /*This is an auxiliary variable to set the initialRotation easier*/
    public Quaternion initialRotation;
    public Vector3 localRotationReference;
    public Vector3 globalRotationReference;

    public void Start()
    {
        initialRotation = Quaternion.Euler(initialRotationEuler);
    }

}
