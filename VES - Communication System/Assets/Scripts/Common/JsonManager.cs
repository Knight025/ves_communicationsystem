﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization;


[Serializable]
public class JsonManager<T> where T : new()
{
    public String nameAppend;
    public T data;

    public JsonManager(String name, T data)
    {
        if (!typeof(T).IsSerializable && !(typeof(ISerializable).IsAssignableFrom(typeof(T))))
            throw new InvalidOperationException("A serializable Type is required");
        this.nameAppend = name;
        this.data = data;
    }

    public JsonManager(String name) : this(name, new T()) { }

    public void SetData(T newData)
    {
        data = newData;
    }

    public T GetData()
    {
        return data;
    }

    public static T LoadDataFromFile(string filePath, String name)
    {
        return JsonUtility.FromJson<T>(File.ReadAllText(filePath + "/" + name + ".json"));
    }

    public void SaveDataToFile(string name, string filePath, bool overwrite)
    {
        String fileName = filePath + "/" + name + "_" + this.nameAppend + ".json";

        if (!overwrite)
        {
            //Avoid overwriting by appending an index to the file name
            int counter = 1;
            while (File.Exists(fileName))
            {
                fileName = filePath + "/" + name + "_" + this.nameAppend + "_" + (counter++).ToString() + ".json"; //e.g. "(filepath)/Alex_2"
            }
        }

        File.WriteAllText(fileName, JsonUtility.ToJson(this, false));
    }
}
