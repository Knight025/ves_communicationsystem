/*
 * networkManager.c
 *
 *  Created on: 14 oct. 2020
 *      Author: sreal
 */

#include "networkManager.h"
#include "esp_log.h"
#include "esp_timer.h"
#include "batteryMonitor.h"
#include "haptics.h"

static const char *NM_TAG = "Network Manager";

typedef enum networkCommand {
	GET_NET_DEVICE_DATA	= 0,
	SYNC_CLOCK			= 1,
	NET_DEVICE_DATA		= 2,
	AQ_SYNC_CLOCK		= 3
} networkCommand_t;


typedef struct __attribute__((__packed__)) newDevicePkg_ {
	uint8_t networkCommand;
	uint8_t deviceID;
	uint8_t hapticInterfaceAvalailable;
	uint8_t moCapAvailable;
	uint32_t batteryLevel;	/*Battery measurement in mV*/
} newDevicePkg_t;


typedef struct netClock_{
	int offset;
	SemaphoreHandle_t mutex;
} netClock_t;

/*Static functions*/
void netClockInit();

/*Global variables*/
static netClock_t netClock;

static uint8_t aqSyncPkg = AQ_SYNC_CLOCK;
static newDevicePkg_t netDevicePkg = {
	.networkCommand = NET_DEVICE_DATA,
	.deviceID = 0,
	.hapticInterfaceAvalailable = 0,
	.moCapAvailable = 1,
	.batteryLevel = 0
};

retval_t networkManagerInit(uint8_t deviceID){
	netClockInit();
	batteryMonitorInit();
	netDevicePkg.deviceID = deviceID;
	return RET_OK;
}

retval_t networkManagerCallback(in_addr_t origin, uint8_t *data, uint16_t length){
	if(data[0] == GET_NET_DEVICE_DATA){
		netDevicePkg.batteryLevel = getBatteryVoltage();
		netDevicePkg.hapticInterfaceAvalailable = hapticsAvailable() ? getHapticsChannel() : 0;
		sendPkg(origin, htons(NETWORK_MANAGER_REMOTE_PORT), (uint8_t*)&netDevicePkg, sizeof(newDevicePkg_t));
	}
	if(data[0] == SYNC_CLOCK){
		int32_t timestampReceived = (int32_t)data[1] + (data[2]<<8) + (data[3]<<16) + (data[4]<<24);
		ESP_LOGI(NM_TAG, "Package length: %d. Timestamp received: %d", length, timestampReceived);
		setNetClock(timestampReceived);
		sendPkg(origin, htons(NETWORK_MANAGER_REMOTE_PORT), &aqSyncPkg, 1);
	}

	return RET_OK;
}


void netClockInit(){
	netClock.mutex = xSemaphoreCreateMutex();
	netClock.offset = 0;
	xSemaphoreGive(netClock.mutex);
}


int getNetClock(){
	int retval;
	xSemaphoreTake(netClock.mutex, portMAX_DELAY);
	retval = netClock.offset + (int)(esp_timer_get_time()/1000);
	xSemaphoreGive(netClock.mutex);
	return retval;
}


retval_t setNetClock(int32_t time){
	xSemaphoreTake(netClock.mutex, portMAX_DELAY);
	netClock.offset = time - esp_timer_get_time()/1000;
	xSemaphoreGive(netClock.mutex);
	return RET_OK;
}
