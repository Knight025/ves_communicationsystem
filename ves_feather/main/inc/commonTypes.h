/*
 * commonTypes.h
 *
 *  Created on: 2 oct. 2020
 *      Author: sreal
 */

#ifndef MAIN_COMMONTYPES_H_
#define MAIN_COMMONTYPES_H_

#include "stdint.h"

typedef enum retval_ {
	RET_OK = 0,
	RET_ERROR = 1,
	RET_TIMEOUT = 2
} retval_t;

typedef struct __attribute__((__packed__)) axis_ {
	int16_t x;
	int16_t y;
	int16_t z;
} axis_t;

#endif /* MAIN_COMMONTYPES_H_ */
