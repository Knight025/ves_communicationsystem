/*
 * haptics.h
 *
 *  Created on: 19 nov. 2020
 *      Author: sreal
 */

#ifndef MAIN_INC_HAPTICS_H_
#define MAIN_INC_HAPTICS_H_

#include "commonTypes.h"
#include "stdBool.h"

retval_t hapticsInit(uint8_t channel);
bool hapticsAvailable();
uint8_t getHapticsChannel();
void fireEffect();
void setVibrationAmplitude(uint8_t value);

#endif /* MAIN_INC_HAPTICS_H_ */
