/*
 * nvs.h
 *
 *  Created on: 6 dic. 2020
 *      Author: sreal
 */

#ifndef MAIN_INC_NVSHANDLER_H_
#define MAIN_INC_NVSHANDLER_H_

#include "commonTypes.h"

retval_t nvsInit();
//retval_t nvsSetU32();
//retval_t nvsSetU16();
//retval_t nvsSetU32();
//retval_t nvsSetU16();
//retval_t nvsSetU8();
//int32_t nvsGetU32();
//int32_t nvsGetU16();
//int32_t nvsGetU8();

retval_t nvsSetAxis(const char* key, axis_t data);
retval_t nvsGetAxis(const char* key, axis_t *out);


#endif /* MAIN_INC_NVSHANDLER_H_ */
