/*
 * haptics.c
 *
 *  Created on: 19 nov. 2020
 *      Author: sreal
 */


#include "haptics.h"
#include <drv2605L/drv2605L.h>
#include <drv2605L/drv2605L_Const.h>
#include "esp_log.h"
#include "virtualScenarioManager.h"
#include "virtualScenarioElements.h"
#include "transportLayer.h"

static const char *HAPTICS_TAG = "Haptics";

static drv2605L_t* drv2605L = NULL;
static vsChannelHandle *hapChannelHandle;
static genList_t *acousticStimElements;
static bool available = false;
static uint8_t hapticsChannel;

static retval_t hapticCallback(uint8_t *data, uint16_t length);
void setVibrationAmplitude(uint8_t value);

retval_t hapticsInit(uint8_t channel){
	drv2605L = drv2605L_create();
	ESP_LOGI("HAPTICS", "Trying to fire element...");
	if(drv2605L_init(drv2605L, I2C_ACTUATOR_1) != RET_OK){
		drv2605L_delete(drv2605L);
		return RET_ERROR;
	}
	drv2605L_exitStandby(drv2605L);
	drv2605L_loadActuatorConfig(drv2605L, &DRV2605L_ACTUATOR1);
	drv2605L_setMode(drv2605L, DRV2605L_MODE_SELECTION1);
	drv2605L_selectEffectLibrary(drv2605L, DRV2605L_LIBRARY1);
	setVibrationAmplitude(128);

	hapChannelHandle = addChannel(channel, hapticCallback);
	acousticStimElements = genListInit();
	hapticsChannel = channel;
	available = true;
	return RET_OK;
}

bool hapticsAvailable(){
	return available;
}

uint8_t getHapticsChannel(){
	return hapticsChannel;
}

void fireEffect(drv2605L_Effect_t hapticEffect){
	ESP_LOGI("HAPTICS", "Firing now...");
	drv2605L_fireROMLibraryEffect(drv2605L, hapticEffect, false);
	return;
}

void setVibrationAmplitude(uint8_t value){
	ESP_LOGI("HAPTICS", "Firing with value: %d", value);
	drv2605L_setRTPInput(drv2605L, value);
	return;
}

//static void runHapticStimuli(acousticStimuli_t* hapticStimuli){
//	if()
//}

static retval_t hapticCallback(uint8_t *data, uint16_t length){
	acStimUpdate_t *stimuli = (acStimUpdate_t*)data;
	if(drv2605L == NULL){
		ESP_LOGE(HAPTICS_TAG, "The driver is not available");
		return RET_ERROR;
	}


//	genListElement_t *current = acousticStimElements->tailElement;
//
//	while(current != NULL){
//		if(((acousticStimuli_t*)current->item)->elementID == stimuli->elementID){
//			((acousticStimuli_t*)current->item)->vibrating = stimuli->vibrating;
//
//			return RET_OK;
//		}
//		current = current->next;
//	}
//	acousticStimuli_t *newElement = (acousticStimuli_t*)pvPortMalloc(sizeof(acousticStimuli_t));
//
//	ESP_LOGI("Haptic Element","Element: %llu, length: %d", stimuli->elementID, length);
	if(stimuli->vibrating > 0){
		setVibrationAmplitude(stimuli->vibrating);
//		testHaptics(stimuli->vibrating);
	}
	return RET_OK;
}
