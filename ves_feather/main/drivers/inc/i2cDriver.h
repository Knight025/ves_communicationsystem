/*
 * i2cDriver.h
 *
 *  Created on: 18 nov. 2020
 *      Author: sreal
 */

#ifndef MAIN_DRIVERS_I2CDRIVER_H_
#define MAIN_DRIVERS_I2CDRIVER_H_

#include <stdio.h>
#include "commonTypes.h"

retval_t i2c_master_init();
retval_t ic2_master_deInit();
retval_t i2c_master_WriteReg(uint8_t devAddress, uint8_t regAddress, uint8_t *buf, uint16_t length);
retval_t i2c_master_ReadReg(uint8_t devAddress, uint8_t regAddress, uint8_t *buf, uint16_t length);


#endif /* MAIN_DRIVERS_I2CDRIVER_H_ */
