/*
 * drv2605L_arch.h
 *
 *  Created on: 19 nov. 2020
 *      Author: sreal
 */

#ifndef MAIN_DRIVERS_DRV2605L_DRV2605L_ARCH_H_
#define MAIN_DRIVERS_DRV2605L_DRV2605L_ARCH_H_

#include "drv2605L.h"
#include "commonTypes.h"

retval_t drv2605_init();

retval_t drv2605L_register_read(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t *buf);

retval_t drv2605L_register_write(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t buf);

retval_t drv2605L_block_write(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t *buf, uint16_t length);


#endif /* MAIN_DRIVERS_DRV2605L_DRV2605L_ARCH_H_ */
