/*
 * lsm6dsox_util.h
 *
 *  Created on: 16 nov. 2020
 *      Author: sreal
 */

#ifndef MAIN_LSM6DSOX_LSM6DSOX_UTIL_H_
#define MAIN_LSM6DSOX_LSM6DSOX_UTIL_H_

#include "lsm6dsox_arch.h"
#include "lsm6dsox_reg.h"

typedef float_t (*IMUconversionFunc)(int16_t lsb);

IMUconversionFunc getGyrConversionData(lsm6dsox_fs_g_t rangeMode);
IMUconversionFunc getAccConversionData(lsm6dsox_fs_xl_t rangeMode);

float getGyrFrequency(lsm6dsox_odr_g_t freq);
float getAccFrequency(lsm6dsox_odr_xl_t freq);

#endif /* MAIN_LSM6DSOX_LSM6DSOX_UTIL_H_ */
