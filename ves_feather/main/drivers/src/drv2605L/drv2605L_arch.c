/*
 * drv2605L_arch.c
 *
 *  Created on: 19 nov. 2020
 *      Author: sreal
 */
#include "drv2605L_arch.h"
#include "i2cDriver.h"

#define DRV2605L_SLAVE_ADDRESS	0x5A
#define DRV2605L_I2C_PORT		I2C_NUM_1
#define DRV2605L_I2C_SDA_IO		23
#define DRV2605L_I2C_SCL_IO		22
#define DRV2605L_I2C_FREQ_HZ	100000

#define I2C_MASTER_TX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define ACK_CHECK_EN 0x1						/*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0						/*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0								/*!< I2C ack value */
#define NACK_VAL 0x1							/*!< I2C nack value */

#define DRV2605L_START_REG					0x01 //0x0FU      Only for debug purposes. Delete afterwards



retval_t drv2605_init(){
	return RET_OK;
}

retval_t drv2605L_register_read(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t *buf)
{
    return i2c_master_ReadReg(DRV2605L_SLAVE_ADDRESS, regAddress, buf, 1);
}

retval_t drv2605L_register_write(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t buf)
{
	return drv2605L_block_write(drvDevice, regAddress, &buf, 1);
}

retval_t drv2605L_block_write(drv2605L_t* drvDevice, drv2605L_Register_t regAddress, uint8_t *buf, uint16_t length)
{
	return i2c_master_WriteReg(DRV2605L_SLAVE_ADDRESS, regAddress, buf, length);
}
