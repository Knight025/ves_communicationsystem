/*
 * lsm6dsl_arch.c
 *
 *  Created on: Jan 31, 2020
 *      Author: sreal
 */
#include "lsm6dsox_arch.h"
#include "driver/i2c.h"
#include <stdio.h>
#include "lsm6dsox_reg.h"
#include "esp_log.h"
#include "i2cDriver.h"

#define LSM6DSOX_SLAVE_ADDRESS	LSM6DSOX_I2C_ADD_L
#define I2C_PORT		I2C_NUM_1
#define I2C_SDA_IO		23
#define I2C_SCL_IO		22
#define I2C_FREQ_HZ	100000

#define I2C_MASTER_TX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define ACK_CHECK_EN 0x1						/*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0						/*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0								/*!< I2C ack value */
#define NACK_VAL 0x1							/*!< I2C nack value */


static const char *IMU_ARCH_TAG = "IMU_ARCH";


int32_t LSM6DSOX_arch_Init(){
	ESP_LOGI(IMU_ARCH_TAG, "I2C Initialization started\n");
//	return i2c_master_init();
	return RET_OK;
}

int32_t LSM6DSOX_arch_DeInit(){
	ESP_LOGI(IMU_ARCH_TAG, "I2C Deinitialization started\n");
//	return ic2_master_deInit();
	return RET_OK;
}

int32_t LSM6DSOX_arch_GetTick(void){
	//What? This is included in the library but seems like it is not used
	return 0;
}

int32_t LSM6DSOX_arch_WriteReg(void* handle, uint8_t regAddress, uint8_t *buf, uint16_t length){
	return i2c_master_WriteReg(LSM6DSOX_I2C_ADD_L, regAddress, buf, length);
}

int32_t LSM6DSOX_arch_ReadReg(void* handle, uint8_t regAddress, uint8_t *buf, uint16_t length){
	return i2c_master_ReadReg(LSM6DSOX_I2C_ADD_L, regAddress, buf, length);
}
