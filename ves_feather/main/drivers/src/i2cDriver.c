/*
 * i2cDriver.c
 *
 *  Created on: 18 nov. 2020
 *      Author: sreal
 */

#include "freertos/FreeRTOS.h"
#include "driver/i2c.h"
#include "i2cDriver.h"
#include "esp_log.h"

//#define LSM6DSOX_SLAVE_ADDRESS	0x6A//LSM6DSOX_I2C_ADD_L
#define I2C_PORT		I2C_NUM_1
#define I2C_SDA_IO		23
#define I2C_SCL_IO		22
#define I2C_FREQ_HZ		200000

#define I2C_MASTER_TX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0				/*!< I2C master doesn't need buffer */
#define ACK_CHECK_EN 0x1						/*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0						/*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0								/*!< I2C ack value */
#define NACK_VAL 0x1							/*!< I2C nack value */


static const char *I2C_TAG = "I2C";

static SemaphoreHandle_t mutex;

retval_t i2c_master_init()
{
	int i2c_master_port = I2C_PORT;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    if( i2c_driver_install(i2c_master_port, conf.mode,
                              I2C_MASTER_RX_BUF_DISABLE,
                              I2C_MASTER_TX_BUF_DISABLE, 0) != ESP_OK){
    	return RET_ERROR;
    }
    mutex = xSemaphoreCreateMutex();
    xSemaphoreGive(mutex);
    return RET_OK;
}

retval_t ic2_master_deInit(){
	if( i2c_driver_delete(I2C_PORT) != ESP_OK)
		return RET_ERROR;
	vSemaphoreDelete(mutex); /*BEWARE!!! THIS might allow a race condition*/
	return RET_OK;
}


retval_t i2c_master_WriteReg(uint8_t devAddress, uint8_t regAddress, uint8_t *buf, uint16_t length){
	uint8_t regAddress_b = regAddress;
	int ret;

	xSemaphoreTake(mutex, portMAX_DELAY);
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
//	i2c_master_write_byte(cmd, LSM6DSOX_SLAVE_ADDRESS << 1 | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, devAddress << 1 | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, regAddress_b, ACK_CHECK_EN);
	while(length-->0){
		i2c_master_write_byte(cmd, *(buf++), ACK_CHECK_EN);
	}
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(I2C_PORT, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	xSemaphoreGive(mutex);
	if (ret != ESP_OK) {
		return ret;
	}

	return 0;
}

retval_t i2c_master_ReadReg(uint8_t devAddress, uint8_t regAddress, uint8_t *buf, uint16_t length){
    int ret;
    uint8_t regAddress_b = regAddress;

    xSemaphoreTake(mutex, portMAX_DELAY);
    //uint8_t sad_r = LSM6DSL_SLAVE_ADDRESS << 1 | I2C_MASTER_WRITE;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    /*First part of the command*/
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, devAddress << 1 | I2C_MASTER_WRITE, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, regAddress_b, ACK_CHECK_DIS);
    /*Second part of the command - Read data*/
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, devAddress << 1 | I2C_MASTER_READ, ACK_CHECK_EN);
    while(length-- > 1){
    	i2c_master_read_byte(cmd, buf++, ACK_VAL);
    }
    i2c_master_read_byte(cmd, buf, NACK_VAL);
    i2c_master_stop(cmd);

    ret = i2c_master_cmd_begin(I2C_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    xSemaphoreGive(mutex);
    if (ret != ESP_OK) {
        return RET_ERROR;
    }

	return RET_OK;
}
